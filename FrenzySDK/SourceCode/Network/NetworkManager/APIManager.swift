
//
// APIContants.swift
// BandPass
//
// Created by Jassie on 12/01/16.
// Copyright © 2016 eeGames. All rights reserved.
//
import Foundation
import SwiftyJSON

typealias APICompletion = (APIResponse) -> ()

class APIManager: NSObject {
    
    static let sharedInstance = APIManager()
    private lazy var httpClient : HTTPClient = HTTPClient()
   
    func opertationWithRequest ( withApi api : API , completion : @escaping APICompletion ) {
       
        self.httpClient.postRequest(withApi: api, success: { (data) in
            guard let response = data else {
                completion(APIResponse.Failure(""))
                return
            }
            let json = JSON(response)
            print("Params: \(api.parameters ?? [String : String]()), URL: \(api.url()), Method: \(api.method), Response: \(json)")
            
            if let blockErrorCode = json.dictionaryValue["errorCode"]?.int , blockErrorCode == 3{
                self.userBlocked_logOut(isFromBlock: true)
                return
            } else if let blockErrorCode = json.dictionaryValue["errorCode"]?.stringValue , blockErrorCode == "3" {
                self.userBlocked_logOut(isFromBlock: true)
                return
            }
           else if let errorCode = json.dictionaryValue["status"]?.int {
                if errorCode == 404 {
                    if let message = json.dictionaryValue["message"]?.stringValue {
                        if message == "Session Expired"{
                            //MARK: Logout functionality
                            return
                        }
                        
                        completion(APIResponse.Failure(message))
                        return
                    }
                    completion(APIResponse.Failure(""))
                    return
                }else if (errorCode == 400) {
                    if let message = json.dictionaryValue["message"]?.stringValue {
                        completion(APIResponse.Failure(message))
                        return
                    }
                }else{
                    if let email = json.dictionaryValue["message"]?["email"][0].stringValue {
                        if !email.isEmpty{
                            completion(APIResponse.Failure(email))
                            return
                        }
                    }
                    if let password = json.dictionaryValue["message"]?["password"][0].stringValue {
                        if !password.isEmpty{
                            completion(APIResponse.Failure(password))
                            return
                        }
                    }
                }
            }else if let status = json.dictionaryValue["status"]?.stringValue {
                if (status == "error") {
                    if let message = json.dictionaryValue["message"]?.stringValue {
                        if message == "Session Expired" {
                            //MARK: Logout functionality
                        }else {
                            completion(APIResponse.Failure(message))
                        }
                        return
                    }
                }
            }
            if json.stringValue.contains("\"status\":\"error\""){
                completion(APIResponse.Failure(""))
                return
            }
            completion(.Success(api.handleResponse(parameters: json)))
       
        }) { (error) in
            
            print(error)
            
            if error.localizedDescription.contains("URLSessionTask failed with error: The Internet connection appears to be offline."){
                completion(APIResponse.Failure("The Internet connection appears to be offline."))

            }else if error.localizedDescription.contains("URLSessionTask failed with error: A data connection is not currently allowed.") {
                completion(APIResponse.Failure("The Internet connection appears to be offline."))
            }
            else{
                completion(APIResponse.Failure(error.localizedDescription))
            }
        }
    }
    
    func opertationWithRequestWithFileUploading ( withApi api : API, image:UIImage , completion : @escaping APICompletion ,pregressComp : @escaping (Double) -> ())  {
           
           httpClient.UploadFiles(withApi: api,image:image,  success: { (data) in
               guard let response = data else {
                   completion(APIResponse.Failure(""))
                   return
               }
               let json = JSON(response)
               print("Json,API : \(json),\(api.url())")
               if let errorCode = json.dictionaryValue["statusCode"]?.int {
                   if  errorCode == 404 {
                       if let message = json.dictionaryValue["errorMessage"]?.stringValue {
                           if message == "Session Expired"{
                               return
                           }
                           
                           completion(APIResponse.Failure(message))
                           return
                       }
                       completion(APIResponse.Failure(""))
                       return
                   }else{
                       if let email = json.dictionaryValue["errorMessage"]?["email"][0].stringValue {
                           if !email.isEmpty{
                               completion(APIResponse.Failure(email))
                               return
                           }
                       }
                       if let password = json.dictionaryValue["errorMessage"]?["password"][0].stringValue {
                           
                           if !password.isEmpty{
                               completion(APIResponse.Failure(password))
                               return
                           }
                           
                           
                       }
                   }
               }else if let status =  json.dictionaryValue["status"]?.stringValue {
                   if (status == "error") {
                       if let message = json.dictionaryValue["errorMessage"]?.stringValue {
                           completion(APIResponse.Failure(message))
                           return
                       }
                   }
               }
               
               if json.stringValue.contains("\"status\":\"error\""){
                   completion(APIResponse.Failure(""))
                   return
               }
               completion(.Success(api.handleResponse(parameters: json)))
               
           },failure:{
    (error) in
               print(error)
            if error.localizedDescription.contains("URLSessionTask failed with error: The Internet connection appears to be offline."){
                completion(APIResponse.Failure("The Internet connection appears to be offline."))

            }else if error.localizedDescription.contains("URLSessionTask failed with error: A data connection is not currently allowed.") {
                completion(APIResponse.Failure("The Internet connection appears to be offline."))
            }
            else{
                completion(APIResponse.Failure(error.localizedDescription))
            }
           }, progress: { (progressDb) in
               print(progressDb)
               pregressComp(progressDb)
           })
       }
    
    func opertationWithRequestWithFileVideoUploading ( withApi api : API, image:UIImage ,video:URL, completion : @escaping APICompletion ,pregressComp : @escaping (Double) -> ())  {
        
//        var videoData = Data()
//        do {
//            videoData = try Data(contentsOf: video)
//            print("video convert into data: \(videoData)")
//        } catch let error {
//            print("video Data convert Error \(error)")
//            completion(APIResponse.Failure(""))
//        }
        
        httpClient.UploadVideo(withApi: api,image:image,videoLink: video,  success: { (data) in
            guard let response = data else {
                completion(APIResponse.Failure(""))
                return
            }
            let json = JSON(response)
            print("Json,API : \(json),\(api.url())")
            if let errorCode = json.dictionaryValue["statusCode"]?.int {
                if  errorCode == 404 {
                    if let message = json.dictionaryValue["errorMessage"]?.stringValue {
                        if message == "Session Expired"{
                            completion(APIResponse.Failure(""))
                            return
                        }
                        completion(APIResponse.Failure(message))
                        return
                    }
                    completion(APIResponse.Failure(""))
                    return
                }else{
                    if let email = json.dictionaryValue["errorMessage"]?["email"][0].stringValue {
                        if !email.isEmpty{
                            completion(APIResponse.Failure(email))
                            return
                        }
                    }
                    if let password = json.dictionaryValue["errorMessage"]?["password"][0].stringValue {
                        if !password.isEmpty{
                            completion(APIResponse.Failure(password))
                            return
                        }
                    }else{
                        completion(APIResponse.Failure(""))
                        return
                    }
                }
            }else if let status =  json.dictionaryValue["status"]?.stringValue {
                if (status == "error") {
                    if let message = json.dictionaryValue["errorMessage"]?.stringValue {
                        completion(APIResponse.Failure(message))
                        return
                    }
                }
            }
            
            if json.stringValue.contains("\"status\":\"error\""){
                completion(APIResponse.Failure(""))
                return
            }
            completion(.Success(api.handleResponse(parameters: json)))
            
        },failure:{
            (error) in
            print(error)
            if error.localizedDescription.contains("URLSessionTask failed with error: The Internet connection appears to be offline."){
                completion(APIResponse.Failure("The Internet connection appears to be offline."))

            }else if error.localizedDescription.contains("URLSessionTask failed with error: A data connection is not currently allowed.") {
                completion(APIResponse.Failure("The Internet connection appears to be offline."))
            }
            else{
                completion(APIResponse.Failure(error.localizedDescription))
            }
        }, progress: { (progressDb) in
            print(progressDb)
            pregressComp(progressDb)
        })
    }
    
    func userBlocked_logOut(isFromBlock: Bool = false){
        isLogOutCalled = true
        let isIntroShown = DataManager.sharedInstance.isIntroScreenNotShown()
        DataManager.sharedInstance.logoutUser()
     
        DataManager.sharedInstance.setIntroScreenShown(isShown: isIntroShown)
        DataManager.sharedInstance.setUserLogout(isLogout: true)
        self.setUserRootController(isUserBlock: isFromBlock)
        IOSocket.sharedInstance.disconnect()
    }
    
    func setUserRootController(isUserBlock:Bool = false){
        FlashFrenzy.offFlash()
        if !isForegroundBlockApi {
            if isUserBlock {
                isForegroundBlockApi = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                   let _ = SweetAlert().showAlert("", subTitle: "You've been removed from Frenzy for violating community guidelines, terms of use, or privacy policy.\nIf you would like to request reinstatement, please contact us at contact@ohheyfrenzy.com.", style: AlertStyle.none, buttonTitle: "Okay", buttonColor: UIColor.colorFromRGB(0xff1a57), buttonTitleColor: .white) { isConfirm in
                        isForegroundBlockApi = false
                    }
                }
            }
        }
    }
    
    //MARK: Get VC From Storyborad
    func getVC(storyboard : Storyboards = Storyboards.MAIN, vcIdentifier : String) -> UIViewController {
        //String = kStoryBoardMain
        return UIStoryboard(name: storyboard.board(), bundle: nil).instantiateViewController(withIdentifier: vcIdentifier)
    }
}



