//
//  AWSManager.swift
//  Frenzy
//
//  Created by CP on 1/21/20.
//  Copyright © 2020 Jhony. All rights reserved.
//

import Foundation
import UIKit
//import AWSS3

//class AWSManager{
//    static var shared  = AWSManager()
//    static var s3Url = AWSS3.default().configuration.endpoint.url
//    
////    func uploadMedia(name : String , extn : String , fileUrl : URL ,completion: @escaping (_ status : Bool ,  _ path:  String) -> Void, progress: ((Double, AWSS3TransferManagerUploadRequest) -> Void)? = nil){//AWSS3TransferManagerUploadRequest//AWSS3TransferUtilityTask
////        let bucketName = kAWSBucketName
////        let key = "\(name).\(extn)"
////        let mngr = AWSS3TransferManagerUploadRequest()
////        mngr?.bucket = bucketName
////        mngr?.key = "public/\(key)"
////        mngr?.body = fileUrl
////        mngr?.acl = .publicReadWrite
////        let transferMngr  = AWSS3TransferManager.default()
////        transferMngr.upload(mngr!).continueWith(executor: AWSExecutor.mainThread()) { (task) -> Any? in
////            if let err = task.error {
////                print(err)
////                completion(false , "")
////            }else if task.result != nil{
////                print(task.result?.absoluteURL as Any)
////                print("Uploaded \(key)")
////                let url = AWSManager.s3Url?.appendingPathComponent(bucketName).appendingPathComponent(mngr!.key!).absoluteString
////                completion(true , url!)
////            }else{
////                completion(false , "")
////            }
////            return nil
////        }
////        mngr?.uploadProgress = {(bytesSent, totalBytesSent, totalBytesExpectedToSend) in
////            DispatchQueue.main.async(execute: {
////                progress?(Double(Double(totalBytesSent)/Double(totalBytesExpectedToSend)), mngr!)
////            })
////        }
////
////    }
//}

extension URL {
    func getFileCompleteName() -> String {
        return self.lastPathComponent
    }
    func getFileName() -> String {
        return String(self.lastPathComponent.split(separator: ".").first!)
    }
    
    func getFileExtension() -> String {
        return String(self.lastPathComponent.split(separator: ".").last!)
    }
}
