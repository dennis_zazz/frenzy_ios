//
//  IOSocket.swift
//  Road Side
//
//  Created by CP on 1/24/19.
//  Copyright © 2019 lucky. All rights reserved.
//

import UIKit
import SocketIO
import Kingfisher
import Network

typealias GetSocketCompletion = ([[String : Any]]) -> ()
typealias SendSocketCompletion = () -> ()

class IOSocket {
    
    let manager = SocketManager(socketURL: URL(string: APIConstants.SocketURL)! , config:[.log(false), .compress , .version(.two)])
    var socket : SocketIOClient?
    static let sharedInstance = IOSocket()
   
    private init(){
        socket = self.manager.defaultSocket
    }
    
    var SocketReceivedCommentFunctionName = "comment_send"
    var SocketSendCommentFunctionName = "comment_get"
    var SocketChatRecieve = "message_send"
    var SocketFrenziesReceive = "event_viewers_update_receive"
    var SocketFrenziesSend = "event_viewers_update_send"
    var SocketMovementsReceive = "event_moments_update_receive"
    var SocketAnimateMovementsReceive = "event_moments_schedule_receive"
    var SocketGoalsReceive = "event_goals_update_receive"
    var SocketPostsReceive = "event_posts_update_receive"
    var SocketEventEndReceive = "event_end_receive"
    var SocketCommentSend = "comment_update_send"
    var SocketCommentReceive = "comment_update_receive"
    var SocketCommentEditSend = "comment_edit_send"
    var SocketCommentEditReceive = "comment_edit_receive"
    var SocketCommentDeleteSend = "comment_delete_send"
    var SocketCommentDeleteReceive = "comment_delete_receive"
    var SocketMomentStop = "moment_end_receive"
    var SocketMomentVideoUpdate  = "moment_video_remove_receive"
    
    var SocketMomentVideoSend = "moment_video_send" // param : viewer_id, viewer_image, viewer_name, moment_id, url, ratio,  thumbnail_img
    var SocketMomentVideoReceive = "moment_video_receive" // param : viewer_id, viewer_image, viewer_name, moment_id, url, ratio,  thumbnail_img
    var SocketVideoLikeSend = "video_like_send" // viewer_id, video_id
    var SocketVideoLikeReceive = "video_like_receive" //viewer_id, video_id
    var SocketVideoUnLikeSend = "video_unlike_send"
    var SocketVideoUnLikeRecive = "video_unlike_receive"
    var SocketViewerBlocked = "viewer_out_event_get"
    var blocked_user_receive = "blocked_user_receive"
    var remove_video_receive = "remove_video_receive"
    
    func sendVideoLikeUnLike(viewer_id:Int,video_id:Int,isLike:Int){
        var item:[String : AnyObject] = [String : AnyObject]()
        item["viewer_id"] = viewer_id as AnyObject
        item["video_id"] = video_id as AnyObject
        if(isLike == 0){
            socket?.emit(SocketVideoUnLikeSend, item)
        }else{
            socket?.emit(SocketVideoLikeSend, item)
        }
    }
    
    func receiverVideo(completion: @escaping GetSocketCompletion){
        socket?.on(SocketMomentVideoReceive) {data, ack in
            let rsp_dt = data as! [[String : Any]]
            completion(rsp_dt)
        }
    }

    func removeBlockVideo(completion: @escaping GetSocketCompletion){
        socket?.on(remove_video_receive) {data, ack in
            let rsp_dt = data as! [[String : Any]]
            completion(rsp_dt)
        }
    }
    
    func receiverLikeVideo(completion: @escaping GetSocketCompletion){
        socket?.on(SocketVideoLikeReceive) {data, ack in
            let rsp_dt = data as! [[String : Any]]
            completion(rsp_dt)
        }
    }
    
    func receiverUnLikeVideo(completion: @escaping GetSocketCompletion){
        socket?.on(SocketVideoUnLikeRecive) {data, ack in
            let rsp_dt = data as! [[String : Any]]
            completion(rsp_dt)
        }
    }
    
    func sendVideoObject(viewer_id:Int,viewer_image:String,viewer_name:String,moment_id:Int,url:String,thumbnail_img:String,id:Int,default_url:String){
        var item:[String : AnyObject] = [String : AnyObject]()
        item["viewer_id"] = viewer_id as AnyObject
        item["viewer_image"] = viewer_image as AnyObject
        item["viewer_name"] = viewer_name as AnyObject
        item["moment_id"] = moment_id as AnyObject
        item["url"] = url as AnyObject
        item["thumbnail_img"] = thumbnail_img as AnyObject
        item["id"] = id as AnyObject
        item["default_url"] = default_url as AnyObject
        socket?.emit(SocketMomentVideoSend, item)
    }

    func Get(completion: @escaping GetSocketCompletion) {
        socket?.on(SocketReceivedCommentFunctionName) {data, ack in
            let rsp_dt = data as! [[String : Any]]
            completion(rsp_dt)
        }
    }
    
    func Send(withParm parm : [String : AnyObject] ,completion: @escaping SendSocketCompletion )  {
        socket?.emit(SocketSendCommentFunctionName, parm)
        completion()
    }
    
    func disconnect(){
        //socket?.disconnect()
    }
    
    func connect() {
        if let status = socket?.status, (status == .disconnected || status == .notConnected){
            print("start to socket connect")
            socket?.connect()
            
        }
               socket?.on(clientEvent: .connect) { data, ack in
                   print(data)
                   print(ack)
                   print("socket connected")
               }
               socket?.on(clientEvent: .disconnect) {data, ack in
                    print("socket disconnect")
               }
               socket?.on("unauthorized") { (data, ack) in
                   print(data)
                   print(ack)
                   print("unauthorized user")
               }
    }
    
    func GetChat(completion: @escaping GetSocketCompletion) {
        socket?.on(SocketChatRecieve) {data, ack in
            print(data)
            let rsp_dt = data as! [[String : Any]]
            completion(rsp_dt)
        }
    }
    
    func sendFriendzies(withParm parm : [String : AnyObject] ,completion: @escaping SendSocketCompletion )  {
        socket?.emit(SocketFrenziesSend, parm)
        completion()
    }
    
    func sendComment(withParm parm : [String : AnyObject] ,completion: @escaping SendSocketCompletion )  {
       // self.connect()
        print("sendComment Socket:\(parm)")
        socket?.emit(SocketCommentSend, parm)
        completion()
    }
    
    func sendCommentEdit(withParm parm : [String : AnyObject] ,completion: @escaping SendSocketCompletion )  {
        socket?.emit(SocketCommentEditSend, parm)
        completion()
    }
    
    func sendCommentDelete(withParm parm : [String : AnyObject] ,completion: @escaping SendSocketCompletion )  {
        socket?.emit(SocketCommentDeleteSend, parm)
        completion()
    }
    
    func getFrenzies(completion: @escaping GetSocketCompletion) {
        socket?.on(SocketFrenziesReceive) {data, ack in
            print(data)
            let rsp_dt = data as! [[String : Any]]
            completion(rsp_dt)
        }
    }
    
    func getComment(completion: @escaping GetSocketCompletion) {
        socket?.on(SocketCommentReceive) {data, ack in
            print(data)
            let rsp_dt = data as! [[String : Any]]
            completion(rsp_dt)
        }
    }
    
    func getCommentEdit(completion: @escaping GetSocketCompletion) {
        socket?.on(SocketCommentEditReceive) {data, ack in
            print(data)
            let rsp_dt = data as! [[String : Any]]
            completion(rsp_dt)
        }
    }
    
    func getCommentDelete(completion: @escaping GetSocketCompletion) {
        socket?.on(SocketCommentDeleteReceive) {data, ack in
            print(data)
            let rsp_dt = data as! [[String : Any]]
            completion(rsp_dt)
        }
    }
    
    func getMovements(completion: @escaping GetSocketCompletion) {
        socket?.on(SocketMovementsReceive) {data, ack in
            print(data)
            let rsp_dt = data as! [[String : Any]]
            completion(rsp_dt)
        }
    }
    
    func getAnimateMovements(completion: @escaping GetSocketCompletion) {
        socket?.on(SocketAnimateMovementsReceive) {data, ack in
            print(data)
            let rsp_dt = data as! [[String : Any]]
            completion(rsp_dt)
        }
    }
    
    func getGoalsCount(completion: @escaping GetSocketCompletion) {
        socket?.on(SocketGoalsReceive) {data, ack in
            print(data)
            let rsp_dt = data as! [[String : Any]]
            completion(rsp_dt)
        }
    }
    
    func getPosts(completion: @escaping GetSocketCompletion) {
        socket?.on(SocketPostsReceive) {data, ack in
            print(data)
            let rsp_dt = data as! [[String : Any]]
            completion(rsp_dt)
        }
    }
    
    func userGetBlockedFromHostPanel(completion: @escaping GetSocketCompletion) {
        socket?.on(SocketViewerBlocked) {data, ack in
            print(data)
            let rsp_dt = data as! [[String : Any]]
            completion(rsp_dt)
        }
    }
    
    func stopMoment(completion: @escaping GetSocketCompletion) {
        socket?.on(SocketMomentStop) {data, ack in
            print(data)
            let rsp_dt = data as! [[String : Any]]
            completion(rsp_dt)
        }
    }
    
    func user_Blocked(completion: @escaping GetSocketCompletion) {
        socket?.on(blocked_user_receive) {data, ack in
            print(data)
            let rsp_dt = data as! [[String : Any]]
            completion(rsp_dt)
        }
    }
    
    func getEventEnd(completion: @escaping GetSocketCompletion) {
        socket?.on(SocketEventEndReceive) {data, ack in
            print(data)
            let rsp_dt = data as! [[String : Any]]
            completion(rsp_dt)
        }
    }
    
    func getVideoUpdate(completion: @escaping GetSocketCompletion) {
           socket?.on(SocketMomentVideoUpdate) {data, ack in
               print(data)
               let rsp_dt = data as! [[String : Any]]
               completion(rsp_dt)
           }
       }
    
}
