//
//  Comment.swift
//  Frenzy
//
//  Created by CodingPixel on 19/03/2020.
//  Copyright © 2020 Jhony. All rights reserved.
//

import Foundation

class Comment: Codable {
    
    var postTime: String? {
        get{
            return createdAt?.getDateFormat(inputFormat: "yyyy-MM-dd HH:mm:ss", outPutFormat: "hh:mm a")
        }
    }
    
    var id: Int? = 0
    var userId: Int? = 0
    var delighterId: Int? = 0
    var comment: String? = ""
    var userType: String? = ""
    var createdAt: String? = ""
    var updatedAt: String? = ""
    var isUpdate: Int? = 0
    var name: String? = ""
    var profileImage: String? = ""
    var isGifUrlInValid:Bool = false
    
    func initComment(from json: [String:Any]) -> Comment {
        let comment = Comment()
        comment.id = json["id"] as? Int ?? 0
        comment.userId = json["user_id"] as? Int ?? 0
        comment.delighterId = json["delighter_id"] as? Int ?? 0
        comment.comment = json["comment"] as? String ?? ""
        if comment.comment!.isEmpty{
            if let intdata =  json["comment"] as? Int {
                comment.comment = "\(intdata)"
            }else{
              comment.comment = "Frenzy!"
            }
        }
        comment.userType = json["user_type"] as? String ?? ""
        comment.createdAt = json["created_at"] as? String ?? ""
        comment.updatedAt = json["updated_at"] as? String ?? ""
        comment.isUpdate = json["is_updated"] as? Int ?? 0
        comment.name = json["name"] as? String ?? ""
        comment.profileImage = json["profile_image"] as? String ?? ""
        return comment
    }
}
