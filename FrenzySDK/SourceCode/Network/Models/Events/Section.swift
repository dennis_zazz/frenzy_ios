//
//  Section.swift
//  Frenzy
//
//  Created by CodingPixel on 03/03/2020.
//  Copyright © 2020 Jhony. All rights reserved.
//

import Foundation

class Section {
    
    var sectionTitle: String? {
        get {
            return title?.trimmingCharacters(in: .whitespaces)
        }
    }
    
    var id: Int? = 0
    var user_id: Int? = 0
    var title: String? = ""
    var event_id: Int? = 0
    var section_tickets: Int? = 0
    var time: String? = ""
    var password: String? = ""
    
    func initSections(from json: [String: Any]) -> Section{
        let section = Section()
        section.id = json["id"] as? Int ?? 0
        section.user_id = json["user_id"] as? Int ?? 0
        section.title = json["title"] as? String ?? ""
        section.event_id = json["event_id"] as? Int ?? 0
        section.section_tickets = json["section_tickets"] as? Int ?? 0
        section.time = json["created_at"] as? String ?? ""
       
        if let pwd = json["password"] as? String{
            section.password = pwd
        }else if let pwd = json["password"] as? Int{
            section.password = "\(pwd)"
        }
        return section
    }
}

extension Section: Equatable{
    
    static func ==(lhs: Section, rhs: Section) -> Bool {
            // Using "identifier" property for comparison
            return lhs.id == rhs.id
        }
}
