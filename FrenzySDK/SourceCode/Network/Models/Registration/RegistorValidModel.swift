//
//  RegistorValidModel.swift
//  Frenzy
//
//  Created by CP on 1/28/20.
//  Copyright © 2020 Jhony. All rights reserved.
//

import Foundation

class RegistrationApiModel: Codable{
    var created_at: String? = ""
    var email: String? = ""
    var email_verified_at: String? = ""
    var id: Int? = -1
    var phone: String? = ""
    var phone_verified_at: String? = ""
    var stickers: [Sticker]? = [Sticker]()
    var updated_at: String? = ""
    var name: String? = ""
    var profile_image: String? = ""
    var device_token: String? = ""
    var noti_android: Int? = 0
    var noti_ios: Int? = 0
    var password: String? = ""
    var is_forgot:Int? = 0
    
}

class RegistrationApiModelProcedure: Codable{
    var created_at: String? = ""
    var email: String? = ""
    var email_verified_at: String? = ""
    var id: Int? = -1
    var phone: Int? = 0
    var phone_verified_at: String? = ""
    var stickers: [Sticker]? = [Sticker]()
    var updated_at: String? = ""
    var name: String? = ""
    var profile_image: String? = ""
    var device_token: String? = ""
    var noti_android: Int? = 0
    var noti_ios: Int? = 0
    var is_sticker_requested: Int? = 0
    
    func getApiModel () -> RegistrationApiModel{
        let item = RegistrationApiModel()
        item.id = self.id
        item.email_verified_at = self.email_verified_at
        item.email = self.email
        item.created_at = self.created_at
        item.phone = "+\(self.phone ?? 0)"
        item.phone_verified_at = self.phone_verified_at
        item.stickers = self.stickers
        item.updated_at = self.updated_at
        item.name = self.name
        item.profile_image = self.profile_image
        return item
    }
}

class Sticker:Codable{
    var address1: String? = ""
    var address2: String? = ""
    var city: String? = ""
    var created_at: String? = ""
    var id: Int? = 0
    var state: String? = ""
    var updated_at: String? = ""
    var viewer_id: Int? = 0
    var zip: String? = ""
}

class Validation:Codable{
    var email_exists: Bool = false
  //var phone_exists: Bool = false
    var user_exists: Bool = false
    var user: RegistrationApiModel?
}

class ValidationLocal:Codable {
    var email:String = ""
    var phone:String = ""
    var name:String = ""
    init(em:String,ph:String,nm:String) {
        self.email = em
        self.phone = ph
        self.name = nm
    }
}
