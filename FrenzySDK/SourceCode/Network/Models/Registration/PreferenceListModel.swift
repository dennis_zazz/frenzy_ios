//
//  PreferenceListModel.swift
//  Frenzy
//
//  Created by Nandini Yadav on 15/06/21.
//  Copyright © 2021 Jhony. All rights reserved.
//

import Foundation

struct PreferenceListModel : Codable {
    var status : String?
    var successMessage : String?
    var successData : [preferenceData]?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case successMessage = "successMessage"
        case successData = "successData"
    }

    init() {
        
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        successMessage = try values.decodeIfPresent(String.self, forKey: .successMessage)
        successData = try values.decodeIfPresent([preferenceData].self, forKey: .successData)
    }

}

struct preferenceData : Codable {
    let id : Int?
    let name : String?
    let image : String?
    let created_at : String?
    let updated_at : String?
    var isSelected: Bool = false
    
    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
        case image = "image"
        case created_at = "created_at"
        case updated_at = "updated_at"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        updated_at = try values.decodeIfPresent(String.self, forKey: .updated_at)
    }

}
