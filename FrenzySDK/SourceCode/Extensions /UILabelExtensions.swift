
import UIKit

extension UILabel{
    
    func setSavedTextTitle(name : String , isSaved : Bool = false) {
        if(!isSaved){
            self.text = name
        }else{
            let attributedString = NSMutableAttributedString(string: name)
            let jeansAttachment = NSTextAttachment()
            jeansAttachment.image = UIImage(named: "pin_red", in: Bundle(for: EventDetailsVC.self), with: nil)
            jeansAttachment.bounds = CGRect(x: 6, y: 0, width: 16, height: 16)
            attributedString.append(NSAttributedString(attachment: jeansAttachment))
            self.attributedText = attributedString
        }
    }
}
