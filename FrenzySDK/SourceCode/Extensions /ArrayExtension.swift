

import Foundation

extension Array where Element == String {
    func removeDuplicates() -> [String] {
        var result = [String]()
        for value in self {
            if !result.contains(value) {
                result.append(value)
            }
        }
        return result
    }
    func difference(from other: [Element]) -> [Element] {
        let thisSet = Set(self)
        let otherSet = Set(other)
        return Array(thisSet.symmetricDifference(otherSet))
    }
}

extension Array where Element == VideoSliderModel {
    
    func removeDuplicates() -> [VideoSliderModel] {
        var result = [VideoSliderModel]()
        for value in self {
            if !result.contains(where: { (itmes) -> Bool in
                return itmes.id == value.id
            }){
                result.append(value)
            }
        }

        return result
    }
}

extension Array where Element: Equatable {

    // Remove first collection element that is equal to the given `object`:
    mutating func remove(object: Element) {
        if let index = firstIndex(of: object) {
            remove(at: index)
        }
    }
}
