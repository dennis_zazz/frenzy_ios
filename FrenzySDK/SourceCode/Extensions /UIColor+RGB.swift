

import UIKit

extension UIColor {
    
    convenience init(red: CGFloat, green: CGFloat, blue: CGFloat) {
        self.init(red: red / 255, green: green / 255, blue: blue / 255, alpha: 1)
    }
    
    convenience init(hexColor: String, alpha: CGFloat = 1.0) {
        if hexColor.count != 6{
            self.init(red:0, green: 0, blue:0, alpha: alpha)
        } else {
            var red: UInt64 = 0, green: UInt64 = 0, blue: UInt64 = 0
            
            let hex = hexColor as NSString
            Scanner(string: hex.substring(with: NSRange(location: 0, length: 2))).scanHexInt64(&red)
            Scanner(string: hex.substring(with: NSRange(location: 2, length: 2))).scanHexInt64(&green)
            Scanner(string: hex.substring(with: NSRange(location: 4, length: 2))).scanHexInt64(&blue)
            self.init(red: CGFloat(red)/255.0, green: CGFloat(green)/255.0, blue: CGFloat(blue)/255.0, alpha: alpha)
        }
    }

    convenience init(hexaString: String, alpha: CGFloat = 1) {
        print("delighter Color:- \(hexaString)")
      if hexaString.hasPrefix("#"){
            let chars = Array(hexaString.dropFirst())
            self.init(red:   .init(strtoul(String(chars[0...1]),nil,16))/255,
                      green: .init(strtoul(String(chars[2...3]),nil,16))/255,
                      blue:  .init(strtoul(String(chars[4...5]),nil,16))/255,
                      alpha: alpha)
      }else if hexaString.isEmpty{
            let chars = Array("FFDD1B")
            self.init(red:   .init(strtoul(String(chars[0...1]),nil,16))/255,
                      green: .init(strtoul(String(chars[2...3]),nil,16))/255,
                      blue:  .init(strtoul(String(chars[4...5]),nil,16))/255,
                      alpha: alpha)
      }else{
          let chars = Array(hexaString)
          self.init(red:   .init(strtoul(String(chars[0...1]),nil,16))/255,
                    green: .init(strtoul(String(chars[2...3]),nil,16))/255,
                    blue:  .init(strtoul(String(chars[4...5]),nil,16))/255,
                    alpha: alpha)
      }
    }

    class var buttonsColor: UIColor {
        return UIColor(red: 41.0/255.0, green: 128.0/255.0, blue: 185.0/255.0, alpha: 1.0)
    }

    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            while !scanner.isAtEnd {
                scanner.currentIndex = scanner.string.index(after: scanner.currentIndex) //scanLocation = 1
            }
        }
        var color: UInt64 = 0
        scanner.scanHexInt64(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }

    
    
}
