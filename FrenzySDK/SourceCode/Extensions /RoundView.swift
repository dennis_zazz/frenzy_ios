

import UIKit

extension UIView {
    
    func  StrainRadious()  {
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 5
        self.layer.borderColor = UIColor.init(red: (107/255), green: (102/255), blue: (93/255), alpha: 1.0).cgColor
        self.clipsToBounds = true
        self.layer.masksToBounds = false
    }
    
    
    func CornerRadious(WithColor : UIColor)  {
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 5
        self.layer.borderColor = WithColor.cgColor
        self.clipsToBounds = true
        self.layer.masksToBounds = false
    }
    
}


