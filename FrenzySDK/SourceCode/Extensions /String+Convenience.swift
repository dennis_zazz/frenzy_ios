
import Foundation
import UIKit
extension String {
    func replaceSpace(str : String) -> String{
        let newString = self.replacingOccurrences(of: " ", with: str)
        return newString
    }
    
    func stringFromCamelCase() -> String {
        var string = self
        string = string.replacingOccurrences(
            of: "([a-z])([A-Z])",
            with: "$1 $2",
            options: .regularExpression,
            range: nil
        )
        string.replaceSubrange(startIndex...startIndex, with: String(self[startIndex]))
        return String(string.prefix(1)).capitalized + String(string.lowercased().dropFirst())
    }
}

extension String {
    func isValidHtmlString() -> Bool {
        if self.isEmpty {
            return false
        }
        return (self.range(of: "<(\"[^\"]*\"|'[^']*'|[^'\">])*>", options: .regularExpression) != nil)
    }
    func decodeUFT8() -> String {
        let data = self.data(using: .utf8)!
        let str = String(data: data, encoding: .nonLossyASCII)
        return str!
    }
    
    func removeSpecialCharsFromString() -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567  890+-*=(),`'.:!_@#")
        return String(self.filter {okayChars.contains($0) })
    }
    func encodeUFT8() -> String {
        guard let  cmt  = self.data(using: .utf8) else  {
            print("comment encodign not success")
            return ""
        }
        let text = String(data: cmt, encoding: .utf8)
        return text ?? ""
    }
    
    
}
