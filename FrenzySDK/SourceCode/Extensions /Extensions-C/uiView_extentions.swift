
import UIKit

private var vBorderColour: UIColor = UIColor.white
private var vCornerRadius: CGFloat = 0.0
private var vBorderWidth: CGFloat = 0.0
private var vMasksToBounds: Bool = true
private var vMakeCircle: Bool = false
private var btnLineSpace: CGFloat = 0.0
private var txtLineSpace: CGFloat = 0.0
private var lblLineSpace: CGFloat = 0.0
@IBDesignable class UIView_Category: UIView   {
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        self.setNeedsLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if makeCircle   {
            layer.cornerRadius = self.bounds.width / 2
        }
        else    {
            layer.cornerRadius = cornerRadius
        }
        layer.borderColor = vBorderColour.cgColor
        layer.borderWidth = vBorderWidth
        layer.masksToBounds = vMasksToBounds
        self.layoutIfNeeded()
    }
    
    override func layoutMarginsDidChange() {
        super.layoutMarginsDidChange()
        self.layoutIfNeeded()
    }
    
    
    
    
}

extension UILabel{
    @IBInspectable var setLineSpacing: CGFloat{
        get {
            return lblLineSpace
        }
        set {
            let textString = NSMutableAttributedString(string: self.text!)
            let textRange = NSRange(location: 0, length: textString.length)
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = newValue
            textString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range: textRange)
            textString.addAttribute(NSAttributedString.Key.kern, value: newValue, range: textRange)
            self.attributedText = textString
            lblLineSpace = newValue
            self.setNeedsLayout()
            
        }
    }
}

extension UIButton{
    @IBInspectable var setLineSpacingButton: CGFloat{
        get {
            return btnLineSpace
        }
        set {
            let textString = NSMutableAttributedString(string: (self.titleLabel?.text ?? "")!)
            let textRange = NSRange(location: 0, length: textString.length)
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = newValue
            textString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range: textRange)
            textString.addAttribute(NSAttributedString.Key.kern, value: newValue, range: textRange)
            self.titleLabel?.attributedText = textString
            btnLineSpace = newValue
            self.setNeedsLayout()
            
        }
    }
}

extension UITextView{
    @IBInspectable var setLineSpacingButton: CGFloat{
        get {
            return txtLineSpace
        }
        set {
            let textString = NSMutableAttributedString(string: self.text!)
            let textRange = NSRange(location: 0, length: textString.length)
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = newValue
            textString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range: textRange)
            textString.addAttribute(NSAttributedString.Key.kern, value: newValue, range: textRange)
            self.attributedText = textString
            txtLineSpace = newValue
            self.setNeedsLayout()
            
        }
    }
}

extension UIView    {
    
    @IBInspectable var cornerRadius: CGFloat{
        get {
            return vCornerRadius
        }
        set {
            layer.cornerRadius = newValue
            vCornerRadius = newValue
            self.setNeedsLayout()
            
        }
    }
    
    @objc func CornerRadious()  {
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.borderColor =  UIColor.clear.cgColor
        self.clipsToBounds = true
        self.layer.masksToBounds = false
    }
    
    func cornerRadiusWithBorder(color: UIColor = #colorLiteral(red: 0.2431372549, green: 0.08235294118, blue: 0.3176470588, alpha: 1) , borderWidth:CGFloat = 1.0){
        self.layer.borderWidth = borderWidth;
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.borderColor =  color.cgColor
        self.clipsToBounds = true
        self.layer.masksToBounds = false
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return vBorderWidth
        }
        set {
            layer.borderWidth = newValue
            vBorderWidth = newValue
            self.setNeedsLayout()
            
        }
    }
    
    @IBInspectable var masksToBounds: Bool {
        get {
            return vMasksToBounds
        }
        set {
            layer.masksToBounds = newValue
            vMasksToBounds = newValue
            self.setNeedsLayout()
            
        }
    }
    
    @IBInspectable var borderColor: UIColor {
        get{
            
            return vBorderColour
        }
        set {
            
            layer.borderColor = newValue.cgColor
            vBorderColour = newValue
            self.setNeedsLayout()
            
        }
    }
    
    @IBInspectable var makeCircle: Bool {
        get{
            
            return vMakeCircle
        }
        set {
            
            if newValue  {
                cornerRadius = frame.size.width / 2
                masksToBounds = true
            }
            else    {
                cornerRadius = vCornerRadius
                masksToBounds = vMakeCircle
            }
            vMakeCircle = newValue
            self.setNeedsLayout()
            
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
                self.setNeedsLayout()
            }
        }
    }
    
    func centerInSuperview() {
        self.centerHorizontallyInSuperview()
        self.centerVerticallyInSuperview()
        self.setNeedsLayout()
        
    }
    func equalAndCenterToSupper() {
        
        self.centerHorizontallyInSuperview()
        self.centerVerticallyInSuperview()
        leadingInSuperview()
        trailingInSuperview()
        topInSuperview()
        bottomInSuperview()
        self.setNeedsLayout()
        
        
    }
    
    func notifyIcon(rightPading : Int = 5 , topPading : CGFloat = 0, color : UIColor = UIColor.init(hexColor: "FA2767")  , size : CGFloat = 8, border: CGFloat = 0){
        let view = UIView()
        view.frame = CGRect.init(x: self.frame.size.width - CGFloat(rightPading), y: topPading, width: size, height: size)
        view.tag = 700
        self.addSubview(view)
        view.backgroundColor = color
        view.cornerRadius = size/2
        view.makeCircle = true
        view.borderWidth = border
        view.borderColor = .black
        view.clipsToBounds = false
        self.updateConstraints()
    }
    
    func removeNotifyIcon(){
        for view in self.subviews{
            if view.tag == 700{
                view.removeFromSuperview()
            }
        }
    }
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat){
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func centerHorizontallyInSuperview(){
        let c: NSLayoutConstraint = NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: self.superview, attribute: .centerX, multiplier: 1, constant: 0)
        self.superview?.addConstraint(c)
    }
    
    func centerVerticallyInSuperview(){
        let c: NSLayoutConstraint = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: self.superview, attribute: .centerY, multiplier: 1, constant: 0)
        self.superview?.addConstraint(c)
    }
    func leadingInSuperview(){
        let c: NSLayoutConstraint = NSLayoutConstraint(item: self, attribute:.leadingMargin, relatedBy: .equal, toItem: self.superview, attribute: .centerY, multiplier: 1, constant: 0)
        self.superview?.addConstraint(c)
    }
    func trailingInSuperview(){
        let c: NSLayoutConstraint = NSLayoutConstraint(item: self, attribute:.trailingMargin, relatedBy: .equal, toItem: self.superview, attribute: .centerY, multiplier: 1, constant: 0)
        self.superview?.addConstraint(c)
    }
    func topInSuperview(){
        let c: NSLayoutConstraint = NSLayoutConstraint(item: self, attribute:.topMargin, relatedBy: .equal, toItem: self.superview, attribute: .centerY, multiplier: 1, constant: 0)
        self.superview?.addConstraint(c)
    }
    func bottomInSuperview(){
        let c: NSLayoutConstraint = NSLayoutConstraint(item: self, attribute:.bottomMargin, relatedBy: .equal, toItem: self.superview, attribute: .centerY, multiplier: 1, constant: 0)
        self.superview?.addConstraint(c)
    }
    
    class func fromNib<T : UIView>() -> T {
        
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    func valueCheck(count:Int) -> CGFloat {
        switch count {
        case 2:
            return 4.0
        case 4:
            return 3.0
        default:
            return 2.0
        }
    }
    
    func genratePatternNumberSeen(width : CGFloat, count : CGFloat ) -> [NSNumber]{
        var array = [NSNumber]()
        for x in 0 ..< Int((count*2)) {
            if(x%2 == 0){
                array.append(NSNumber.init(value: Int(width)))
            }else{
                array.append(2)
            }
        }
        return array
    }
    
    func genratePatternNumberUnSeen(width : CGFloat, count : CGFloat , remainCount : CGFloat) -> [NSNumber]{
        var array = [NSNumber]()
        for x in 0 ..< Int((count*2)-1) {
            if(x%2 == 0){
                array.append(NSNumber.init(value: Int(width)))
            }else{
                array.append(2)
            }
        }
        let nmbr = (width*remainCount) + (valueCheck(count: Int(count+remainCount))*remainCount)
        array.append(NSNumber.init(value: Float(nmbr)))
        return array
    }
    func dottedLine(color: UIColor = #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1), storiesCount: CGFloat = 1, unseenCount: CGFloat = 0, unSeenColor: UIColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)){
        for layer in self.layer.sublayers!{
            if(layer.isKind(of: CAShapeLayer.self)){
                layer.removeFromSuperlayer()
            }
        }
        if(unseenCount == 0){
            let shapeLayer:CAShapeLayer = CAShapeLayer()
            let frameSize = self.frame.size
            let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width - 5, height: frameSize.height - 5)
            let dashwidth = ((shapeRect.width - 4)*3.14)/storiesCount
            shapeLayer.bounds = shapeRect
            shapeLayer.position = CGPoint(x: (frameSize.width)/2 , y: frameSize.height/2)
            shapeLayer.fillColor = UIColor.clear.cgColor
            shapeLayer.strokeColor = UIColor.lightGray.cgColor
            shapeLayer.lineWidth = 1.5
            shapeLayer.lineJoin = CAShapeLayerLineJoin.round
            shapeLayer.lineDashPattern = genratePatternNumberSeen(width: dashwidth, count: storiesCount)
            shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 25).cgPath
            self.layer.addSublayer(shapeLayer)
        }else if (storiesCount == 1){
            if(unseenCount == 0){
                let strokeColor2 = color.cgColor
                let shapeLayer:CAShapeLayer = CAShapeLayer()
                let frameSize = self.frame.size
                let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width - 5, height: frameSize.height - 5)
                let dashwidth = (shapeRect.width)*3.14
                shapeLayer.bounds = shapeRect
                shapeLayer.position = CGPoint(x: (frameSize.width)/2 , y: frameSize.height/2)
                shapeLayer.fillColor = UIColor.clear.cgColor
                shapeLayer.strokeColor = strokeColor2
                shapeLayer.lineWidth = 1.5
                shapeLayer.lineJoin = CAShapeLayerLineJoin.round
                shapeLayer.lineDashPattern = genratePatternNumberSeen(width: dashwidth, count: storiesCount)
                shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 25).cgPath
                self.layer.addSublayer(shapeLayer)
            }else{
                let shapeLayer:CAShapeLayer = CAShapeLayer()
                let frameSize = self.frame.size
                let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width - 5, height: frameSize.height - 5)
                let dashwidth = (shapeRect.width)*3.14
                shapeLayer.bounds = shapeRect
                shapeLayer.position = CGPoint(x: (frameSize.width)/2 , y: frameSize.height/2)
                shapeLayer.fillColor = UIColor.clear.cgColor
                shapeLayer.strokeColor = UIColor.lightGray.cgColor
                shapeLayer.lineWidth = 1.5
                shapeLayer.lineJoin = CAShapeLayerLineJoin.round
                shapeLayer.lineDashPattern = genratePatternNumberSeen(width: dashwidth, count: storiesCount)
                shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 25).cgPath
                self.layer.addSublayer(shapeLayer)
            }
        }else if(unseenCount == storiesCount){
            let strokeColor2 = color.cgColor
            let shapeLayer:CAShapeLayer = CAShapeLayer()
            let frameSize = self.frame.size
            let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width - 5, height: frameSize.height - 5)
            let dashwidth = ((shapeRect.width - 4)*3.14)/storiesCount
            shapeLayer.bounds = shapeRect
            shapeLayer.position = CGPoint(x: (frameSize.width)/2 , y: frameSize.height/2)
            shapeLayer.fillColor = UIColor.clear.cgColor
            shapeLayer.strokeColor = strokeColor2
            shapeLayer.lineWidth = 1.5
            shapeLayer.lineJoin = CAShapeLayerLineJoin.round
            shapeLayer.lineDashPattern = genratePatternNumberSeen(width: dashwidth, count: storiesCount)
            shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 25).cgPath
            self.layer.addSublayer(shapeLayer)
        }else {
            let shapeLayer:CAShapeLayer = CAShapeLayer()
            let frameSize = self.frame.size
            let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width - 5, height: frameSize.height - 5)
            let dashwidth = ((shapeRect.width - 4)*3.14)/storiesCount
            shapeLayer.bounds = shapeRect
            shapeLayer.position = CGPoint(x: (frameSize.width)/2 , y: frameSize.height/2)
            shapeLayer.fillColor = UIColor.clear.cgColor
            shapeLayer.strokeColor = UIColor.lightGray.cgColor
            shapeLayer.lineWidth = 1.5
            shapeLayer.lineJoin = CAShapeLayerLineJoin.round
            shapeLayer.lineDashPattern = genratePatternNumberSeen(width: dashwidth, count: storiesCount)
            shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 25).cgPath
            self.layer.addSublayer(shapeLayer)
            
            let strokeColor2 = color.cgColor
            let shapeLayer2:CAShapeLayer = CAShapeLayer()
            let frameSize2 = self.frame.size
            let shapeRect2 = CGRect(x: 0, y: 0, width: frameSize2.width - 5, height: frameSize2.height - 5)
            shapeLayer2.bounds = shapeRect2
            shapeLayer2.position = CGPoint(x: (frameSize2.width)/2 , y: frameSize2.height/2)
            shapeLayer2.fillColor = UIColor.clear.cgColor
            shapeLayer2.strokeColor = strokeColor2
            shapeLayer2.lineWidth = 1.5
            shapeLayer2.lineJoin = CAShapeLayerLineJoin.round
            shapeLayer2.lineDashPattern = genratePatternNumberUnSeen(width: dashwidth, count: (storiesCount-unseenCount), remainCount:  unseenCount)
            shapeLayer2.path = UIBezierPath(roundedRect: shapeRect2, cornerRadius: 25).cgPath
            self.layer.addSublayer(shapeLayer2)
        }
    }
    
    
    func dotted(color: UIColor = UIColor(red: 44/225, green: 52/225, blue: 65/225, alpha: 0.8)){
        let color = color.cgColor
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width - 5, height: frameSize.height - 5)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: (frameSize.width)/2 , y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 1.5
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [5,1] as [NSNumber]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
    
    func hepticEffect(){
        let impactFeedbackgenerator = UIImpactFeedbackGenerator(style: .heavy)
        impactFeedbackgenerator.prepare()
        impactFeedbackgenerator.impactOccurred()
    }
    
}


extension UIView {
    
    enum UIViewFadeStyle {
        case bottom
        case top
        case left
        case right
        
        case vertical
        case horizontal
    }
    
    func fadeView(style: UIViewFadeStyle = .bottom, percentage: Double = 0.07) {
        let gradient = CAGradientLayer()
        gradient.frame = bounds
        gradient.colors = [UIColor.white.cgColor, UIColor.clear.cgColor]
        let startLocation = percentage
        let endLocation = 1 - percentage
        switch style {
        case .bottom:
            gradient.startPoint = CGPoint(x: 0.5, y: endLocation)
            gradient.endPoint = CGPoint(x: 0.5, y: 1)
        case .top:
            gradient.startPoint = CGPoint(x: 0.5, y: startLocation)
            gradient.endPoint = CGPoint(x: 0.5, y: 0.0)
        case .vertical:
            gradient.startPoint = CGPoint(x: 0.5, y: 0.0)
            gradient.endPoint = CGPoint(x: 0.5, y: 1.0)
            gradient.colors = [UIColor.clear.cgColor, UIColor.white.cgColor, UIColor.white.cgColor, UIColor.clear.cgColor]
            gradient.locations = [0.0, startLocation, endLocation, 1.0] as [NSNumber]
            
        case .left:
            gradient.startPoint = CGPoint(x: startLocation, y: 0.5)
            gradient.endPoint = CGPoint(x: 0.0, y: 0.5)
        case .right:
            gradient.startPoint = CGPoint(x: endLocation, y: 0.5)
            gradient.endPoint = CGPoint(x: 1, y: 0.5)
        case .horizontal:
            gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
            gradient.colors = [UIColor.clear.cgColor, UIColor.white.cgColor, UIColor.white.cgColor, UIColor.clear.cgColor]
            gradient.locations = [0.0, startLocation, endLocation, 1.0] as [NSNumber]
        }
        layer.mask = gradient
    }
    
    //
    // MARK: SHIMMER EFFECT
    enum DirectionShimmer: Int {
        case topToBottom = 0
        case bottomToTop
        case leftToRight
        case rightToLeft
    }
    
    func startShimmeringAnimation(animationSpeed: Float = 1.4,
                                  direction: DirectionShimmer = .leftToRight,
                                  repeatCount: Float = MAXFLOAT) {
        
        // Create color  ->2
        let lightColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 0.1).cgColor
        let blackColor = UIColor.black.cgColor
        
        // Create a CAGradientLayer  ->3
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [blackColor, lightColor, blackColor]
        gradientLayer.frame = CGRect(x: -self.bounds.size.width, y: -self.bounds.size.height, width: 3 * self.bounds.size.width, height: 3 * self.bounds.size.height)
        
        switch direction {
        case .topToBottom:
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
            
        case .bottomToTop:
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
            
        case .leftToRight:
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
            
        case .rightToLeft:
            gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.5)
        }
        
        gradientLayer.locations =  [0.35, 0.50, 0.65] //[0.4, 0.6]
        self.layer.mask = gradientLayer
        
        // Add animation over gradient Layer  ->4
        CATransaction.begin()
        let animation = CABasicAnimation(keyPath: "locations")
        animation.fromValue = [0.0, 0.1, 0.2]
        animation.toValue = [0.8, 0.9, 1.0]
        animation.duration = CFTimeInterval(animationSpeed)
        animation.repeatCount = repeatCount
        CATransaction.setCompletionBlock { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.layer.mask = nil
        }
        gradientLayer.add(animation, forKey: "shimmerAnimation")
        CATransaction.commit()
    }
    
    func stopShimmeringAnimation() {
        self.layer.mask = nil
    }
    func isShimmeringAnimation() -> Bool {
        return self.layer.mask != nil
    }
    
    
}

extension UIButton {
    func setTitleWithoutAnimation(title: String?) {
        UIView.setAnimationsEnabled(false)
        self.setTitle(title, for: .normal)
        layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
}

