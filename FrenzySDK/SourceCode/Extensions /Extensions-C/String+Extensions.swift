

import Foundation
import UIKit
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG
import FLAnimatedImage
protocol StringType { var get: String { get } }
extension String: StringType { var get: String { return self } }
extension Optional where Wrapped: StringType {
    func unwrap() -> String {
        return self?.get ?? ""
    }
}





extension String {
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self.trimmingCharacters(in: .whitespaces))
    }
    var doubleValue: Double {
        return (self as NSString).doubleValue
    }
    
    func boldString(fontSize : CGFloat ,font : UIFont?) -> NSMutableAttributedString {
        let attrs = [NSAttributedString.Key.font : font ?? UIFont.systemFont(ofSize: 8)]
        return NSMutableAttributedString(string:self, attributes:attrs)
    }
    
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        
        return String(data: data, encoding: .utf8)
    }
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    
    func getDateFormat(inputFormat:String,outPutFormat:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inputFormat
      
        let formatter = DateFormatter()
        formatter.dateFormat = outPutFormat
       
        if let dt = dateFormatter.date(from: self)?.toLocalTime() {
            let mnth_name =  formatter.string(from: dt)
            return mnth_name
       
        }else{
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.sssZ"
            dateFormatter.timeZone = TimeZone(identifier: "UTC")
            
            formatter.timeZone = TimeZone.current
            if let dt = dateFormatter.date(from: self){
                let mnth_name =  formatter.string(from: dt)
                return mnth_name
            }
          return self
        }
       
    }
    
    func getRemainingSeconds(inputFormat: String) -> Int? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inputFormat
        if let dt = dateFormatter.date(from: self)?.toLocalTime() {
            let currentDate = Date()
            let timeInterval = Int(dt.timeIntervalSince(currentDate))
            return timeInterval
        }
        return nil
    }
    
  /*  func MD5() -> Data {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        let messageData = self.data(using:.utf8)!
        var digestData = Data(count: length)
        
        _ = digestData.withUnsafeMutableBytes { digestBytes -> UInt8 in
            messageData.withUnsafeBytes { messageBytes -> UInt8 in
                if let messageBytesBaseAddress = messageBytes.baseAddress, let digestBytesBlindMemory = digestBytes.bindMemory(to: UInt8.self).baseAddress {
                    let messageLength = CC_LONG(messageData.count)
                    CC_MD5(messageBytesBaseAddress, messageLength, digestBytesBlindMemory)
                }
                return 0
            }
        }
        return digestData
    }
    
   func md5Encryption() -> String{
        let md5Data = self.MD5()
        return md5Data.map { String(format: "%02hhx", $0) }.joined()
    } */
    
    func getAgeFormDOB() -> String{
        let now = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMMM-yyyy"
        let birthday: Date = formatter.date(from: self)!
        let calendar = Calendar.current
        
        let ageComponents = calendar.dateComponents([.year], from: birthday, to: now)
        let age = ageComponents.year!
        return String(age)
    }
    
    
    func base64toImage() -> UIImage {
        if let data = Data(base64Encoded: self) {
            return UIImage(data: data)!
        }else{
            return UIImage.init()
        }
    }
    
    func base64toGif() -> FLAnimatedImage {
          if let data = Data(base64Encoded: self) {
            return FLAnimatedImage.init(gifData: data)
          }else{
              return FLAnimatedImage.init()
          }
      }
    
    
    
}

extension String {
    
    var parseJSONString: AnyObject? {
        let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)
        if let jsonData = data {
            // Will return an object or nil if JSON decoding fails
            do{
                if let json = try (JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary){
                        return json
                }else{
                let json = try (JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSArray)
                        return json
                }
            }catch {
                print("Error")
            }
            
        } else {
            // Lossless conversion of the string was not possible
            return nil
        }
        return nil
}
    
    var parseJSONStringArray: AnyObject? {
        
        let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)
        let json:NSArray
        
        if let jsonData = data {
            // Will return an object or nil if JSON decoding fails
            do{
                json  = try  JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers)  as! NSArray
                
                return json
            }catch{
                print("Error")
            }
            
        } else {
            // Lossless conversion of the string was not possible
            return nil
        }
        
        return nil
    }
}
extension Date {
    func toString( dateFormat format  : String) -> String
          {
              let dateFormatter = DateFormatter()
              dateFormatter.dateFormat = format
              dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
              return dateFormatter.string(from: self)
        }
}
extension String
{
    func toDate( dateFormat format  : String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        return dateFormatter.date(from: self)!
    }

   
    func UTCToLocal(inputFormate : String , outputFormate : String) -> String {
        if self.count > 0 {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat =  inputFormate  //Input Format "yyyy-MM-dd HH:mm:ss"
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
            let UTCDate = dateFormatter.date(from: self)
            dateFormatter.dateFormat =  outputFormate // Output Format "MM.dd.yyyy hh:mm a"
            dateFormatter.timeZone = .current
            let UTCToCurrentFormat = dateFormatter.string(from: UTCDate!)
            print(UTCToCurrentFormat)
            return UTCToCurrentFormat
        }else{
            return "Empty Date!"
        }
    }
    func getRanges(of string: String) -> [NSRange] {
        var ranges:[NSRange] = []
        if contains(string) {
            let words = self.components(separatedBy: " ")
            var position:Int = 0
            for word in words {
                if word.lowercased() == string.lowercased() {
                    let startIndex = position
                    let endIndex = word.count
                    let range = NSMakeRange(startIndex, endIndex)
                    ranges.append(range)
                }
                position += (word.count + 1)
            }
        }
        return ranges
    }
    
    func getRemidersRemainingDays() -> String{
        let dateRangeStart = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dt = dateFormatter.date(from: self)
//        dt = dt?.toLocalTime()
        let calendar = Calendar.current
        if calendar.isDateInTomorrow(dt!) {
            return "Tomorrow"
        }else if  calendar.isDateInToday(dt!){
             return "Today"
        }else{
            var diffInDays = calendar.dateComponents([.day], from: dateRangeStart, to: dt!).day
            if diffInDays! > 0 {
                diffInDays = (diffInDays!) + 1
            }
            if diffInDays! <= 0{
                return "Today"
            }else{
                return "\(String(describing: diffInDays!)) Days"
            }
        }
    }
    
}
extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    var extractURLs: [URL] {
        var urls : [URL] = []
        var error: NSError?
        do{
            let detector = try NSDataDetector.init(types: NSTextCheckingResult.CheckingType.link.rawValue)
            let text = self
            detector.enumerateMatches(in: text, range: NSMakeRange(0, text.count), using: { (result: NSTextCheckingResult!, flags: NSRegularExpression.MatchingFlags, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
                //            println("\(result)")
                //            println("\(result.URL)")
                urls.append(result.url!)
            })
        }catch let error1 as NSError {
            error = error1
            print(error!.description)
        } catch {
            // Catch any other errors
            print(error.localizedDescription)
        }
            
            return urls
        
    }
}

extension NSMutableAttributedString {
    
    func setColorForText(textForAttribute: String, withColor color: UIColor) {
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)
        
        // Swift 4.2 and above
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
        
        // Swift 4.1 and below
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    }
    
}
