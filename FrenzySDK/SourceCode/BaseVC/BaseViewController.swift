//
//  File.swift
//
//
//  Created by Nandini Yadav on 20/12/21.
//

import Foundation
import UIKit
import Kingfisher
import KingfisherWebP
import UICircularProgressRing
//import AASquaresLoading

var isLogOutCalled:Bool = false
var isForegroundBlockApi: Bool = false
var isAllowBothOriantations = false
var localLatitude:Double = 0
var localLongtitude:Double = 0
var isPostReceived = false
var isMomentReceived = false
var isAppInBackground = false
var refreshHomeScreen = true
var momentEndIds = [Int]()
var selectedMomentID = 0

class BaseViewController: UIViewController{
    
    //MARK: Constraint Keys
    @IBOutlet weak var backImgHeightConstraint:NSLayoutConstraint!
    @IBOutlet weak var backImgWidthConstraint:NSLayoutConstraint!
  
    //MARK: Delay - Setup Top Bar White Or Black Icon
    var isNeedToDark = false
    var loadingView : UIView?
    
    var localUser : User? {
        get{
            DataManager.sharedInstance.getPermanentlySavedUser()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        overrideUserInterfaceStyle = .light
        if(isNeedToDark){
            return .lightContent
        }else{
            return .default
        }
    }
    
    func setupBettryIconBlackOrWhite(isBlack:Bool = true){
        self.isNeedToDark = !isBlack
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        KingfisherManager.shared.cache.clearMemoryCache()
        KingfisherManager.shared.cache.clearDiskCache()
        KingfisherManager.shared.cache.cleanExpiredDiskCache()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    deinit {
  
    }
    
    @IBOutlet weak var lblTitleHeader:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            //UIApplication.shared.applicationIconBadgeNumber = 0
        }
        if(self.backImgHeightConstraint != nil && self.backImgWidthConstraint != nil){
            self.backImgHeightConstraint.constant = 28
            self.backImgWidthConstraint.constant = 28
        }
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(self.appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.appCameFromBackGround), name: UIApplication.didBecomeActiveNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.logOut), name: NSNotification.Name.init(SideMenuItems.kLogOut), object: nil)
        // Do any additional setup after loading the view.
        
        IOSocket.sharedInstance.connect()
        socketsReceiver()
        let bundle = Bundle(for: HomeVC.self)
        let fonts = bundle.urls(forResourcesWithExtension: "ttf", subdirectory: nil)
        fonts?.forEach({ url in
            CTFontManagerRegisterFontsForURL(url as CFURL, .process, nil)
        })
    }
    
    func onMomentEnd(id : Int) {
        print("Base View Controller")
    }
    
    func updateNotificaitonCount() {
//        APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.updateNotifcation()), completion: { apiResponse in
//            print(apiResponse)
//        })
    }
    
    func socketsReceiver() {
       
        IOSocket.sharedInstance.userGetBlockedFromHostPanel(completion: { (result) in
            print("user Get Blocked FromHostPanel")
            if isLogOutCalled {
                return
            }
            print(result)
            if result.count > 0 {
                var eventID = 0
                if let id = result.first!["event_id"] as? String{
                    eventID = Int(id) ?? 0
                }
                else if let id = result.first!["event_id"] as? Int{
                    eventID = id
                }
                let viewerId = result.first!["viewer_id"] as? String ??  "\(result.first!["viewer_id"] as? Int ?? 0)"
                
                var eventIdArr = UserDefaults.standard.array(forKey: kEventIdKey) as? [Int] ?? [Int]()
                print("All event id Arr: \( eventIdArr)")
                if eventIdArr.contains(eventID) , let userId = self.localUser?.userItem.id , "\(userId)" == viewerId{
                    eventIdArr.remove(object: eventID)
                    UserDefaults.standard.set(eventIdArr, forKey: kEventIdKey)
                    UserDefaults.standard.synchronize()
                    let _ = SweetAlert().showAlert("", subTitle: "You've been removed from the event by the event host for violating community guidelines, terms of use, or privacy policy.\nIf you would like to request reinstatement, please contact us at contact@ohheyfrenzy.com.", style: AlertStyle.none, buttonTitle: "Okay", buttonColor: UIColor.colorFromRGB(0xff1a57), buttonTitleColor: .white) { isConfirm in
                        self.setHomeRootVC()
                    }
                }
            }
        })
        
        IOSocket.sharedInstance.user_Blocked { (result) in
            print("blocked User: \(result)")
            let userId = self.localUser?.userItem.id ?? 0

            if let id = result.first!["user_id"] as? Int , userId == id {
                self.logOut(isFromBlock: true)

            }else if let id = result.first!["user_id"] as? String , "\(userId)" == id {
                self.logOut(isFromBlock: true)
            }
        }
//
        IOSocket.sharedInstance.stopMoment(completion: { result in
            if isLogOutCalled {
                return
            }
            if result.count > 0 {
                if let id = result.first!["moment_id"] as? Int{
                    self.onMomentEnd(id: id)
                    momentEndIds.append(id)
                } else if let id = result.first!["moment_id"] as? String{
                    let myInt3 = (id as NSString).integerValue
                    self.onMomentEnd(id: myInt3)
                    momentEndIds.append(myInt3)
                }
            }
        })
        
        IOSocket.sharedInstance.getPosts(completion: { result in
            if isLogOutCalled {
                return
            }
            if !isPostReceived {
                isPostReceived = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    isPostReceived = false
                })
                if result.count > 0 {
                    let eventIdArr = UserDefaults.standard.array(forKey: kEventIdKey) as? [Int] ?? [Int]()
                    let sectionIdArr = UserDefaults.standard.array(forKey: kSectionIdKey) as? [Int] ?? [Int]()
                    let userId = DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0
                    let list = DataManager.sharedInstance.getFarAwayEvents()
                  
                    for eventId in eventIdArr {
                        var eventID = ""
                      
                        if let id = result.first!["event_id"] as? String{
                            eventID = id
                        }
                        else if let id = result.first!["event_id"] as? Int{
                            eventID = "\(id)"
                        }
                        
                        if "\(eventId)" ==  eventID {
                            
                            let delighter = Delighter().initDelighter(from: result.first!)
                            let sectionsString = delighter.sections.components(separatedBy: ",")
                            let sections = sectionsString.compactMap { Int($0) }
                    
                            if (delighter.event_type == "remote") || isGlobalDemoEvent {
                                print("Demo and remote delighter")
                                self.openDelighterVC(with: delighter, openBase64: false , eventID: eventID)
                            
                            }
                            else if (delighter.isForFarAway == "1" && list.contains(eventId)){
                                print("isForFarAway delighter")
                                self.openDelighterVC(with: delighter, openBase64: false , eventID: eventID)
                            }
                            else if delighter.isGround == "1" && !list.contains(eventId){
                                print("isGround ")
                                if sections.count > 0 {
                                    for sec in sections {
                                        if sec == userId {
                                            self.openDelighterVC(with: delighter, openBase64: false , eventID: eventID)
                                            break
                                        }else{
                                            for mySec in sectionIdArr {
                                                if sec == mySec {
                                                    self.openDelighterVC(with: delighter, openBase64: false , eventID: eventID)
                                                    break
                                                }
                                            }
                                        }
                                    }
                                }else{
                                    print("sections is empty")
                                    self.openDelighterVC(with: delighter, openBase64: false , eventID: eventID)
                                }
                            }
                                break
                            }
                        }
                    }
                }
        })
        
        IOSocket.sharedInstance.getAnimateMovements(completion: { result in
            print("get Animate Movements")
            if isLogOutCalled {
                return
            }
            if !isMomentReceived {
                isMomentReceived = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    isMomentReceived = false
                })
                if result.count > 0 {
                    let savedArray = DataManager.sharedInstance.getAdditionalDataForAnimateMoment()
                    var evventId = 0
                    var bgColor = FrenzyColor.blackKindColor
                    var animatetext = ""
                    var textColor  = FrenzyColor.frozyKindColor
                    var seatNumber  = "0"
                    let totalDuration = (result.first!["created_at"] as? String)!
                    let time = (result.first!["date_time"] as? String)!
                    if let event_id = result.first!["event_id"] as? Int,
                        let moment_level = result.first!["level"] as? String,
                        let moment_rows = result.first!["rows"] as? String
                    {
                        evventId = event_id
                        let rows = moment_rows.components(separatedBy: ",")
                        for obj in savedArray{
                            if obj.event_id == event_id && obj.level == moment_level && rows.contains(obj.row){
                                let color = result.first!["color"] as? String ?? "000000"
                                if !color.isEmpty {
                                    if color.contains("#"){
                                        bgColor = UIColor.init(hexColor: String(color.dropFirst()))
                                    }else{
                                        bgColor = UIColor.init(hexColor: String(color))
                                    }
                                }
                                animatetext = result.first!["text"] as? String ?? "FRENZY"
                                animatetext = animatetext.capitalized
                                
                                let text_color = result.first!["text_color"] as? String ?? "000000"
                                if !text_color.isEmpty {
                                    if text_color.contains("#"){
                                        textColor = UIColor.init(hexColor: String(text_color.dropFirst()))
                                    }else{
                                        textColor = UIColor.init(hexColor: String(text_color))
                                    }
                                }
                                seatNumber = obj.seatNumber!
                                break
                            }
                        }
                    }
                    self.openAnimateMoment(eventID: evventId, text: animatetext, bgColor: bgColor, textColor: textColor, seatNumber: seatNumber, time: time, duration : totalDuration)
                }
                
            }
        })
        
        IOSocket.sharedInstance.getMovements(completion: { result in
            print("Movement socket called")
            if isLogOutCalled {
                return
            }
            if !isMomentReceived {
                isMomentReceived = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    isMomentReceived = false
                })
                if result.count > 0 {
                    let eventIdArr = UserDefaults.standard.array(forKey: kEventIdKey) as? [Int] ?? [Int]()
                    let sectionIdArr = UserDefaults.standard.array(forKey: kSectionIdKey) as? [Int] ?? [Int]()
                    let flashArr = UserDefaults.standard.array(forKey: kFlashKey) as? [Bool] ?? [Bool]()
                    let screenArr = UserDefaults.standard.array(forKey: kScreenKey) as? [Bool] ?? [Bool]()
                    let vibrateArr = UserDefaults.standard.array(forKey: kVibrateKey) as? [Bool] ?? [Bool]()
                    
                    for (index,eventId) in eventIdArr.enumerated() {
                        var eventID = ""
                      
                        if let id = result.first!["event_id"] as? String{
                            eventID = id
                        }
                        else if let id = result.first!["event_id"] as? Int{
                            eventID = "\(id)"
                        }
                        if "\(eventId)" ==  eventID {
                            let movement = Movement().initMovementFrom(json: result.first!)
                            let sectionsString = movement.sections.components(separatedBy: ",")
                            let sections = sectionsString.compactMap { Int($0) }
                            let list = DataManager.sharedInstance.getFarAwayEvents()
                            let nowDuratioin = movement.remainingDuration
                            
                            if(movement.event_type == "remote") || isGlobalDemoEvent{
                                print("remote")
                                let userPermission = UserPermissionInfo(flash: flashArr[index], screen: screenArr[index], vibrate: vibrateArr[index])
                                self.openMovementVC(with: movement, userPermission: userPermission ,duration: nowDuratioin ,  eventId: eventID)
                            }
                            else if (movement.isForFarAway == 1 && list.contains(eventId)){
                                print("isForFarAway")
                                let userPermission = UserPermissionInfo(flash: flashArr[index], screen: screenArr[index], vibrate: vibrateArr[index])
                                self.openMovementVC(with: movement, userPermission: userPermission , duration: nowDuratioin ,eventId: eventID)
                            }
                            else if movement.isGround == 1 && !list.contains(eventId){
                                for sec in sections {
                                        for mySec in sectionIdArr {
                                            if sec == mySec {
                                                let userPermission = UserPermissionInfo(flash: flashArr[index], screen: screenArr[index], vibrate: vibrateArr[index])
                                                self.openMovementVC(with: movement, userPermission: userPermission, duration: nowDuratioin ,eventId: eventID , mySec : sec)
                                                break
                                            }
                                        }
                                }
                            }
                            break
                        }
                        }
                    }
                }
            })
    }
    
    func setHomeRootVC(){
        print("User Get Blocked, Exit from SDK")
        if let navVC = self.navigationController?.viewControllers , navVC.count > 0{
            for vc in navVC{
                if vc.isKind(of: HomeVC.self){
                    self.navigationController?.popToViewController(vc, animated: true)
                }
            }
        }
    }
    
    func openEventScreen(id : Int, type : String) {
        self.showLoading(on: self.view, of: FrenzyColor.grayKindColor)
        APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.getEventDetailsFromMomentorDelighter( id: id, getType: type)), completion: { apiResponse in
            self.hideLoading(on: self.view)
            switch apiResponse {
            case .Failure(_):
                if let nev = self.navigationController{
                    nev.popViewController(animated: true)
                }else{
                    self.dismiss(animated: true, completion: nil)
                }
                break
                
            case .Success(let data):
                if let itemListNew = data?.result as? [EventItem]{
                   
                    switch itemListNew[0].subscriptionStatus {
                    case .NONE:
                        print("Open Base ViewController NONE EventMovementVC")
                        if isGlobalDemoEvent{
                            self.navigationController?.popViewController(animated: false)
                        }
                        else{
                            let vc = EventMovementVC.instantiate(fromAppStoryboard: .home)
                            vc.eventItem = itemListNew[0]
                            vc.eventTitle = itemListNew[0].title ?? ""
                          //  (vc as! EventMovementVC).eventItem = itemListNew[0]
                          //  (vc as! EventMovementVC).eventTitle = itemListNew[0].title ?? ""
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    case .PENDING:
                        //show event ticket page
                        let feed = EventTicketVC.instantiate(fromAppStoryboard: .home)
                        feed.eventItem = itemListNew[0]
                        self.navigationController?.pushViewController(feed, animated: true)
                        
                    case .COMPLETED:
                        
                        switch itemListNew[0].eventStatus {
                        case .UPCOMING:
                            //show countDown Screen
                            let feed = EventCountDownVC.instantiate(fromAppStoryboard: .home)
                            feed.eventItem = itemListNew[0]
                            self.navigationController?.pushViewController(feed, animated: true)
                        case .ONGOING:
                            print("Open Base ViewController ONGOING EventMovementVC")
                            //show movement screen
                            if isGlobalDemoEvent{
                                self.navigationController?.popViewController(animated: false)
                            }else{
                                let vc = EventMovementVC.instantiate(fromAppStoryboard: .home)
                                vc.eventItem = itemListNew[0]
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        case .ENDED:
                            //show end result screen
                            print("Open Base ViewController EventSummeryVC")
                            let vc = EventSummeryVC.instantiate(fromAppStoryboard: .Movement)
                            vc.eventItem = itemListNew[0]
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }else{
                    if let nev = self.navigationController{
                        nev.popViewController(animated: true)
                    }else{
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                break
            }
        })
    }
    
    @objc func logOut(isFromBlock: Bool = false){
        isLogOutCalled = true
        let isIntroShown = DataManager.sharedInstance.isIntroScreenNotShown()
        DataManager.sharedInstance.logoutUser()
     
        DataManager.sharedInstance.setIntroScreenShown(isShown: isIntroShown)
        DataManager.sharedInstance.setUserLogout(isLogout: true)
        self.setUserRootController(isUserBlock: isFromBlock)
        IOSocket.sharedInstance.disconnect()
    }
    
    func setUserRootController(isUserBlock:Bool = false){
        FlashFrenzy.offFlash()
//            let nav = UINavigationController(rootViewController: self.getVC(storyboard: Storyboards.MAIN, vcIdentifier: SplashVC.identifier))
//            nav.isNavigationBarHidden = true
//            let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
//            appDelegate.window?.rootViewController = nav
//            appDelegate.window?.makeKeyAndVisible()
        
        if !isForegroundBlockApi {
            if isUserBlock {
                isForegroundBlockApi = true
                self.delay(2) {
                    let _ = SweetAlert().showAlert("", subTitle: "You've been removed from Frenzy for violating community guidelines, terms of use, or privacy policy.\nIf you would like to request reinstatement, please contact us at contact@ohheyfrenzy.com.", style: AlertStyle.none, buttonTitle: "Okay", buttonColor: UIColor.colorFromRGB(0xff1a57), buttonTitleColor: .white) { isConfirm in
                        isForegroundBlockApi = false
                    }
                }
            }
        }
    }
    
    func checkUserIsLoggedIn()->Bool{
        if isLogOutCalled || DataManager.sharedInstance.getUserLogout(){
            self.showErrorToast(title: "Session Expired!", description: "Please Login Again", completation: { isDone in
                self.logOut()
            })
            self.logOut()
            return false
        }else{
            return true
        }
    }
    
    @objc func appMovedToBackground() {
        print("App moved to background!")
        isAppInBackground = true
        isForegroundBlockApi = false
    }
    
    @objc func appCameFromBackGround(){
        print("App moved to foreground!")
        isAppInBackground = false
        IOSocket.sharedInstance.connect()
        checkUserIsBlocked_FromApp()
    }
    
    func showSuccessPopUp(_ completion : @escaping (Bool) -> Void){
        let vc = SuccessVC.instantiate(fromAppStoryboard: .Alert)
        vc.popUpDismissWithAcceptence = { isAgree in
            completion(isAgree)
        }
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(vc, animated: true, completion: nil)
    }
    
    func showSettingPopUp(isVenuHide:Bool = false ,_ completion : @escaping (Int) -> Void){
        let vc = SettingPopUp.instantiate(fromAppStoryboard: .Alert)
        vc.settingAction = {index in
            completion(index)
        }
        vc.isVenuHide = isVenuHide
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(vc, animated: true, completion: nil)
    }
    
    func showLoaderFile(_ completion : @escaping (UploadFilePopUp) -> Void){
        let vc = UploadFilePopUp.instantiate(fromAppStoryboard: .Movement)
        vc.providesPresentationContextTransitionStyle = true
        vc.definesPresentationContext = true
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(vc, animated: true, completion: nil)
        completion(vc)
    }
    
    
    func openAnimateMoment(eventID : Int, text : String , bgColor  : UIColor , textColor : UIColor, seatNumber : String, time : String , duration : String)  {
        let vc = AnimationTextVC.instantiate(fromAppStoryboard: .Alert)
        vc.seatNumber =  Int(seatNumber)!
        vc.bg_Color = bgColor
        vc.text_Color = textColor
        vc.animateText = text
        vc.time = time
        vc.event_id = eventID
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickMenu(_ sender : UIButton){
        IOSocket.sharedInstance.disconnect()
        FrenzySDK.shared.delegate?.frenzyDidEndSDK()
        self.navigationController?.popViewController(animated: true)
       // if self.checkUserIsLoggedIn() {
//            let vc = self.getVC(storyboard: Storyboards.HOME, vcIdentifier: MenuVC.identifier)
//            self.navigationController?.pushViewController(vc, animated: true)
      //  }
    }
    
    @IBAction func onClickSearch(_ sender : UIButton){
        if self.checkUserIsLoggedIn(){
            let searchVC = SearchVC.instantiate(fromAppStoryboard: .home)
            self.navigationController?.pushViewController(searchVC, animated: true)
        }
    }
    
    func openUrl(_ urlLink : String){
        var link = ""
        if urlLink.hasPrefix("http://") || urlLink.hasPrefix("https://"){
            link = urlLink
        }else{
            link = "https://\(urlLink)"
        }
        
        if let url = URL(string: link), UIApplication.shared.canOpenURL(url) {//
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    //MARK: Delay
    func delay(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
    //MARK: Get VC From Storyborad
    func getVC(storyboard : Storyboards = Storyboards.MAIN, vcIdentifier : String) -> UIViewController {
        return UIStoryboard(name: storyboard.board(), bundle: nil).instantiateViewController(withIdentifier: vcIdentifier)
    }
    
    @IBAction func onClickDismiss(_ sender : Any ){
        if let nev = self.navigationController{
            nev.popViewController(animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK: Set SIngle Color text
    func  setAttributedTextForLabel(mainString : String , attributedStringsArray : [String] , lbl : UILabel , color : UIColor, attFont:UIFont) {
        let attributedString1    = NSMutableAttributedString(string: mainString)
        for objStr in attributedStringsArray {
            let range1 = (mainString as NSString).range(of: objStr)
            let attribute_font = [NSAttributedString.Key.font: attFont]
            attributedString1.addAttributes(attribute_font, range:  range1)
            attributedString1.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range1)
        }
        lbl.attributedText = attributedString1
    }
    
    //MARK: Set multi Color text
    func  setAttributedTextForLabel(mainString : String , attributedStringsArray : [String] , lbl : UILabel , color : [UIColor], attFont:[UIFont]) {
       
        let attributedString1    = NSMutableAttributedString(string: mainString)
        for (index,objStr) in attributedStringsArray.enumerated() {
            let range1 = (mainString as NSString).range(of: objStr)
            let attribute_font = [NSAttributedString.Key.font: attFont[index]]
            attributedString1.addAttributes(attribute_font, range:  range1)
            attributedString1.addAttribute(NSAttributedString.Key.foregroundColor, value: color[index], range: range1)
        }
        lbl.attributedText = attributedString1
    }
    
    // MARK: Error Pop Up
    func showLoading(on view: UIView, of color: UIColor, autoShowView: Bool = false) {
        DispatchQueue.main.async {
            let loadingView = UIView(frame: view.bounds)
            loadingView.backgroundColor = UIColor.clear
            let activityIndicator = UIActivityIndicatorView(style: .large)
            activityIndicator.style = .large
            activityIndicator.color = color
            activityIndicator.startAnimating()
            activityIndicator.center = view.center
            loadingView.addSubview(activityIndicator)
            view.addSubview(loadingView)
            self.loadingView = loadingView
        }
    }
    
    func hideLoading(on view: UIView, autoHideView: Bool = false) {
        DispatchQueue.main.async {
            self.loadingView?.removeFromSuperview()
            self.loadingView = nil
        }
    }
    
    func getDemoDelighterAndMoment(_ isOnDismiss:Bool = false , eventID: Int?){
       
        var apiUrl: API?
        if isOnDismiss{
            apiUrl = API.clearDemoEvent(event_id: "\(eventID ?? 0)")
        }
        else if demoEventMomentType == .moment , !isMomentApiCalled{
            print("Api moment")
            isMomentApiCalled = true
            apiUrl = API.demoMoment(event_id: "\(eventID ?? 0)")
        }
        else if demoDelighterNum == 1 , demoEventMomentType == .delighter, !isFirstDelighterApiCalled{
            isFirstDelighterApiCalled = true
            print("Api first Delighter")
            apiUrl = API.demoDelighter(del_number: "1", event_id: "\(eventID ?? 0)")
        }
        else if demoEventMomentType == .delighter , demoDelighterNum == 2,!isSecondDelighterApiCalled {
            print("Api second Delighter")
            isSecondDelighterApiCalled = true
            apiUrl = API.demoDelighter(del_number: "2", event_id: "\(eventID ?? 0)")
        }
        else if demoEventMomentType == .delighter , demoDelighterNum == 3 ,!isThirdDelighterApiCalled {
            print("Api third Delighter")
            isThirdDelighterApiCalled = true
            apiUrl = API.demoDelighter(del_number: "3", event_id: "\(eventID ?? 0)")
        }
        else{
            return
        }
        guard let api = apiUrl else{ return}
        APIManager.sharedInstance.opertationWithRequest(withApi: api, completion: { apiResponse in

            switch apiResponse {
           
            case .Failure(_):
                break
           
            case .Success(let data):
                guard let results = data?.result as? [[String:Any]] else {
                    return
                }
               print(results)
                break
            }
        })
    }
    
    @objc func confirmationCustomeAlert(title : String , discription : String , btnColor1: UIColor = UIColor.colorFromRGB(0x2e84a5), btnColor2: UIColor = UIColor.colorFromRGB(0xD0D0D0), btnTitle1 : String = "YES" , btnTitle2 : String = "NO" ,complition : @escaping(Bool,Int) -> Void) {
        
        SweetAlert().showAlert(title, subTitle: discription, style: AlertStyle.none, buttonTitle:btnTitle2, buttonColor:btnColor2 , otherButtonTitle:  btnTitle1, otherButtonColor: btnColor1) { (isOtherButton) -> Void in
            if isOtherButton == true {
                complition(false,2)
            }  else {
                complition(true,1)
            }
        }
    }
    
    func showPasswordAlert(_ eventName: String , passowrd: String , completion: @escaping((_ done:Bool? , _ cancel: Bool?) -> Void)){
        
        SweetAlert().showAlert("", subTitle: "Enter password to join \(eventName)", style: AlertStyle.none, buttonTitle:"CANCEL", buttonColor:UIColor.colorFromRGB(0xffffff) , otherButtonTitle:  "SUBMIT", otherButtonColor: UIColor.colorFromRGB(0xff1a57) , true , passowrd)  { (isOtherButton) -> Void in
         
            if isOtherButton == true {
                completion(nil, true)
            }
            else {
                print("Submit")
                completion(true, nil)
            }
        }
    }
    
    func openMovementVC(with movement: Movement, userPermission: UserPermissionInfo, duration : Int = 0 , isFromTblSelection: Bool = false , eventId: String = "" , mySec:Int = 0) {
        NotificationCenter.default.post(name: .exitDemoMoment, object: nil)
      
        if !eventId.isEmpty, let demoEventId = UserDefaults.standard.value(forKey: "demo_event") as? Int , "\(demoEventId)" != eventId {
           exitDemoEvent()
        }
        if isDemoEventClear {
            print("Demo event has been cleared")
            isDemoEventClear = false
            self.openEventMovement(with: movement, userPermission: userPermission ,duration: duration , isFromTblSelection : isFromTblSelection,  eventId: eventId , sec: mySec)
        }
        else {
            self.openEventMovement(with: movement, userPermission: userPermission ,duration: duration , isFromTblSelection : isFromTblSelection,  eventId: eventId , sec: mySec)
        }
    }
    
    func openEventMovement(with movement: Movement, userPermission: UserPermissionInfo, duration : Int = 0 , isFromTblSelection: Bool = false , eventId: String = "" , sec:Int = 0 ){
        self.delay(0.1) {
            print("vc = \(self.nibName ?? "mm")")
            print("vc moment id = \(movement.id)")
            print("vc moment time = \(movement.duration)")
            if momentEndIds.contains(movement.id){
                return
            }
            let state = UIApplication.shared.applicationState
            if state != .background {
                let vc = MovementWaveVC.instantiate(fromAppStoryboard: .Movement)
                vc.movement = movement
                vc.event_id = eventId
                vc.userPermission = userPermission
                vc.duration = duration
                vc.mySelectedSection = sec
                vc.isFromTblSelection = isFromTblSelection
                if self.navigationController != nil{
                    self.navigationController?.pushViewController(vc, animated: false)
                }else{
                    if let lastVC = (UIApplication.shared.windows.first?.rootViewController as? UINavigationController)?.viewControllers.last {
                        if let baseVc =  lastVC  as?  BaseViewController {
                            baseVc.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
            }
        }
    }

    func exitDemoEvent(){
        if isGlobalDemoEvent {
            if let controllers = self.navigationController?.viewControllers{
                for vc in controllers{
                    if vc.isKind(of: EventMovementVC.self) , let eventMovementvc = vc as? EventMovementVC{
                        FlashFrenzy.offFlash()
                        isDemoEventClear = false
                        print("Exit demo event")
                        isGlobalDemoEvent = false
                        eventMovementvc.dismissEventMovementVC(true)
                        demoDelighterNum = 1
                        demoEventMomentType = .delighter
                        isMomentApiCalled = false
                        isFirstDelighterApiCalled = false
                        isSecondDelighterApiCalled = false
                        isThirdDelighterApiCalled = false
                        demoEventId = 0
                        UserDefaults.standard.removeObject(forKey: "demo_event")
                        break
                    }
                }
            }
        }
    }
    
    func checkUserIsBlocked(_ eventId: Int? , completion : @escaping (Bool) -> ()){
        self.showLoading(on: self.view, of: FrenzyColor.grayKindColor)
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.check_blocked(event_id: "\(eventId ?? 0)", viewer_id: "\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0)"), completion: { apiResponse in
            
            self.hideLoading(on: self.view)
            
            switch apiResponse {
            case .Failure(_):
                break
                
            case .Success(let data):
                guard let results = data?.result as? [String:Any] else {
                    return
                }
                print(results)
                if let status = results["status"] as? String{
                    if status == "true"{
                        completion(true)
                    }else{
                        completion(false)
                    }
                }
                break
            }
        })
    }
    
    func checkUserIsBlocked_FromApp(){
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.check_Blocked_viewer(email: DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.email ?? ""), completion: { apiResponse in
                        
            switch apiResponse {
            case .Failure(_):
               // print("\(error)")
                break
                
            case .Success(let data):
                guard let results = data?.result as? [String:Any] else {
                    return
                }
                print(results)
                break
            }
        })
    }
    
    func openDelighterVC(with delighter: Delighter, openBase64: Bool = false, isFromTimeLine : Bool = false , isFromTblSelection: Bool = false , eventID: String = "" , bottomColor: String = "") {
        
        NotificationCenter.default.post(name: .exitDemoMoment, object: nil)
       
        if !eventID.isEmpty , let demoEventId = UserDefaults.standard.value(forKey: "demo_event") as? Int , "\(demoEventId)" != eventID{
            exitDemoEvent()
        }
        if isDemoEventClear {
            print("Demo event has been cleared")
            isDemoEventClear = false
            openEventDelighterVC(with: delighter, openBase64: openBase64, isFromTimeLine: isFromTimeLine, isFromTblSelection: isFromTblSelection, eventID: eventID , bottomColor : bottomColor)
        }
        else{
            self.delay(0.1) {
                self.openEventDelighterVC(with: delighter, openBase64: openBase64, isFromTimeLine: isFromTimeLine, isFromTblSelection: isFromTblSelection, eventID: eventID , bottomColor : bottomColor)
            }
        }
    }
    
    func openEventDelighterVC(with delighter: Delighter, openBase64: Bool = false, isFromTimeLine : Bool = false , isFromTblSelection: Bool = false , eventID: String = "" , bottomColor:String = ""){
        
        let vc = MovementTextVC.instantiate(fromAppStoryboard: .Movement)
        vc.delighter = delighter
        vc.openBase64Img = openBase64
        vc.isFromTimeLine = isFromTimeLine
        vc.isFromTblSelection = isFromTblSelection
        vc.event_Id = eventID
        vc.bottomColor = bottomColor
        if self.navigationController != nil{
            print("Delighter Nav is nil")
            self.navigationController?.pushViewController(vc, animated: false)
        }else{
            if let lastVC = (UIApplication.shared.windows.first?.rootViewController as? UINavigationController)?.viewControllers.last {
                if let baseVc =  lastVC  as?  BaseViewController {
                    baseVc.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    func openSettingsVC(with eventItem: EventItem) {
        let vc = EventDetailsVC.instantiate(fromAppStoryboard: .home)
        vc.eventItem = eventItem
        vc.isFromSettings = true
        vc.eventLogOutDone = {
            self.delay(0.5, completion: {
                print("Base view controller")
                self.logOut()
            })
        }
        self.present(vc, animated: true, completion: nil)
    }
}
