//
//  VideoCacheManager.swift
//  Duet
//
//  Created by Jassie on 8/2/19.
//  Copyright © 2019 CodingPixel. All rights reserved.
//

import UIKit

public enum ResultVideo<T> {
    case success(T)
    case failure(NSError)
}

public enum MultipleResultVideos<T> {
    case success([T])
    case failure(Error)
}

class CacheManager {
    
    static let shared = CacheManager()
    private let fileManager = FileManager.default
    private lazy var mainDirectoryUrl: URL = {
        let documentsUrl = self.fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first!
        return documentsUrl
    }()
    
    func getFileWith(stringUrl: String, completionHandler: @escaping (ResultVideo<URL>) -> Void ) {
        guard let file = directoryFor(stringUrl: stringUrl) else {return}
        //return file path if already exists in cache directory
        if !fileManager.fileExists(atPath: file.path){
            completionHandler(ResultVideo.failure(NSError.init()))
        }  else {
            completionHandler(ResultVideo.success(file))
            return
        }
        
        DispatchQueue.global().async {
            if let videoData = NSData(contentsOf: URL(string: stringUrl)!) {
                videoData.write(to: file, atomically: true)
            }
        }
    }
    
    func getMultipleFilesWith(stringUrls: [String], localURLs: [URL] = [],completionHandler: @escaping (MultipleResultVideos<URL>) -> Void ) {
        
        if localURLs.count == stringUrls.count {
            completionHandler(MultipleResultVideos.success(localURLs))
        } else {
            var cacheUrls = localURLs
            cacheFile(from: stringUrls[localURLs.count], completionHandler: { result in
                switch result {
                    case .success(let url):
                        print("success")
                        cacheUrls.append(url)
                        self.getMultipleFilesWith(stringUrls: stringUrls,localURLs: cacheUrls, completionHandler: { result in
                            switch result {
                            case .success(let urls):
                                print("success multiple")
                                completionHandler(MultipleResultVideos.success(urls))
                            case .failure(_):
                                print("failure multiple")
                                completionHandler(MultipleResultVideos.failure(NSError.init()))
                            }
                        })
                    
                    case .failure(_):
                        print("failure")
                        completionHandler(MultipleResultVideos.failure(NSError.init()))
                }
            })
        }
    }
    
    func cacheFile(from stringUrl: String, completionHandler: @escaping (ResultVideo<URL>) -> Void) {
        guard let file = directoryFor(stringUrl: stringUrl) else {return}
        //return file path if already exists in cache directory
        if fileManager.fileExists(atPath: file.path){
            completionHandler(ResultVideo.success(file))
            return
        }  else {
            DispatchQueue.global().async {
                if let videoData = NSData(contentsOf: URL(string: stringUrl)!) {
                    if videoData.write(to: file, atomically: true) {
                        completionHandler(ResultVideo.success(file))
                    } else {
                        completionHandler(ResultVideo.failure(NSError.init()))
                    }
                }
            }
        }
    }
    
    private func directoryFor(stringUrl: String) -> URL? {
        guard let fileURL = URL(string: stringUrl)?.lastPathComponent else {
            print("url not found")
            return nil
        }
        let file = self.mainDirectoryUrl.appendingPathComponent(fileURL)
        
        return file
    }
}
