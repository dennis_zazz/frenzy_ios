//
//  StoryBoardExtension.swift
//  Jane
//
//  Created by Mayank Purwar on 13/04/20.
//  Copyright © 2020 Mayank Purwar. All rights reserved.
//

import Foundation
import UIKit

enum FrenzyAppStoryboard : String {
    // These enum name has to match with the storyboard file name
    case main = "FrenzyMain"
    case home = "FrenzyHome"
    case Movement = "FrenzyMovement"
    case Alert = "FrenzyAlert"

    var instance : UIStoryboard {
        //let bundle = Bundle(identifier: "com.FrenzySDK")
        let bundle = Bundle(for: HomeVC.self)
        return UIStoryboard(name: self.rawValue, bundle: bundle)
    }
    
    // Sample usage:
    func viewController<T: UIViewController>(viewControllerClass: T.Type) -> T {
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
        return instance.instantiateViewController(withIdentifier: storyboardID) as! T
    }
    
    func initialViewController() -> UIViewController? {
        return instance.instantiateInitialViewController()
    }
    
}

extension UIViewController {
    
    // The Storyboard ID for the initial View Controller has to be defined with the same name as the view controller name
    class var storyboardID : String {
        return "\(self)"
    }
    static func instantiate(fromAppStoryboard appStoryboard: FrenzyAppStoryboard) -> Self {
        return appStoryboard.viewController(viewControllerClass: self)
    }
    
    class func topViewController(controller: UIViewController? = UIApplication.shared.windows.first(where: \.isKeyWindow)?.rootViewController) -> UIViewController? {
      
     //   let window =  UIApplication.shared.windows.first(where: \.isKeyWindow)?.rootViewController

        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

