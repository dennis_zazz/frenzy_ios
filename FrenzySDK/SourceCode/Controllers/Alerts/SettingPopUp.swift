//
//  SettingPopUp.swift
//  Frenzy
//
//  Created by Jawad on 6/29/20.
//  Copyright © 2020 Jhony. All rights reserved.
//

import UIKit

class SettingPopUp: BaseViewController {
    static let identifier = "SettingPopUp"
    var settingAction:((Int) -> ())?
    
    @IBOutlet weak var uiChangeVenu:UIView!
    var isVenuHide:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.uiChangeVenu.isHidden = isVenuHide
    }
    @IBAction func onClickSetting(_ sender: Any) {
        self.dismiss(animated: true) {
            self.settingAction?(1)
        }
    }
    
    @IBAction func onClickAttending(_ sender: Any) {
        self.dismiss(animated: true) {
            self.settingAction?(2)
        }
    }
    
    @IBAction func onClickExit(_ sender: Any) {
        self.dismiss(animated: true) {
            self.settingAction?(3)
        }
    }
}
