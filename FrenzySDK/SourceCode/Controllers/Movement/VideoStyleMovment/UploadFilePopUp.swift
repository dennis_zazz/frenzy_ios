//
//  UploadFilePopUp.swift
//  Frenzy
//
//  Created by CP on 5/13/20.
//  Copyright © 2020 Jhony. All rights reserved.
//

import UIKit
import KDCircularProgress

class UploadFilePopUp: BaseViewController {

    static let identifier = "UploadFilePopUp"
    @IBOutlet weak var progress:KDCircularProgress!
    @IBOutlet weak var lblProgress:UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.progress.progress = 0
        self.lblProgress.text = "0%"
        self.progress.set(colors: UIColor.init(hexColor: "D41C4E") ,UIColor.init(hexColor: "D41C4E"), UIColor.init(hexColor: "D41C4E"), UIColor.init(hexColor: "D41C4E"), UIColor.init(hexColor: "D41C4E"))
        // Do any additional setup after loading the view.
    }
    
    func setupProgress(_ progress:Double){
        if(!isBeingDismissed){
            self.lblProgress.text = progress == 100 ? "100" : "\(Int.init((progress * 100).rounded(FloatingPointRoundingRule.towardZero)))%"
            self.progress.progress = (progress)
        }
    }


}
