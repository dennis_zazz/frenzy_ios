
import UIKit
import AVFoundation
import AVKit
//import RecordButton

class CameraVC: BaseViewController, AVCaptureFileOutputRecordingDelegate {
   
    static let identifier = "CameraVC"
    var outputURL: URL!
    @IBOutlet weak var btnRecord: UIButton!//RecordNewButton!
    @IBOutlet weak var dummyView: UIView!
    @IBOutlet weak var btnRecordHeight: NSLayoutConstraint!
    @IBOutlet weak var btnRecordWidth: NSLayoutConstraint!
    @IBOutlet weak var redView: UIView!
    var previewLayer: AVCaptureVideoPreviewLayer?
    var movieOutput : AVCaptureMovieFileOutput?
    var captureSession : AVCaptureSession?
    var defaultVideoDevice: AVCaptureDevice?
    var activeInput: AVCaptureDeviceInput?
    @IBOutlet weak var lblTimer:UILabel!
    @IBOutlet weak var cameraView: UIView!
    
// Change for rotate camera
    @IBOutlet weak var rotateCameraButton: UIButton!
    
    var timer: Timer?
    var totalTime = 4
    var momentId:Int? = 0
    var eventId:Int? = 0
    var videoAdded:((VideoSliderModel) -> Void)?
    var videoUploaded: (()->Void)?
    var onDissMiss:(() -> Void)?
    var isCameraRotate: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        if setupSession() {
            setupPreview()
            startSession()
        }
        btnRecord.addTarget(self, action: #selector(self.record), for: .touchUpInside)
        btnRecord.isHidden = false
        self.view.bringSubviewToFront(btnRecord)
        IOSocket.sharedInstance.connect()
        IOSocket.sharedInstance.stopMoment(completion: { result in
                    if isLogOutCalled {
                        return
                    }
                    if result.count > 0 {
                        if self.eventId == (result.first!["event_id"] as! Int) &&  self.momentId  == (result.first!["moment_id"] as! Int){
                            self.onClickDismiss(UIButton())
                        }
                    }
                })
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.layer.layoutIfNeeded()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.record()
    }
    
    override func appCameFromBackGround(){
        print("Camera permission")
        
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized && isAudioAuthorized() {
            //already authorized
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                DispatchQueue.main.async {
                    if !granted {
                        print("Access denied")
                        self.cameraSettingPopup()
                    }else{
                        AVCaptureDevice.requestAccess(for: .audio) { (granted: Bool) in
                            DispatchQueue.main.async {
                                if !granted {
                                    print("Audio Access denied")
                                    self.cameraSettingPopup()
                                }
                            }
                        }
                    }
                }
            })
        }
    }
    
    func isAudioAuthorized() -> Bool {
        return AVCaptureDevice.authorizationStatus(for: .audio) == .authorized
    }
    
    override func appMovedToBackground() {
        if btnRecord.isSelected{
            btnRecord.isSelected = false
            self.timer?.invalidate()
            self.totalTime = 4
            self.lblTimer.text = "00:05"
        }
        isCameraRotate = true
        stopRecording()
    }
    
    @objc func startAnimation(){
        //startTutorialANimation(tutorialCommentContainerView, nil)
    }
    
    func startRecording() {
        
        if let output = movieOutput , output.isRecording == false {
           
            let connection = movieOutput?.connection(with: AVMediaType.video)
            
            if let connection = connection , connection.isVideoOrientationSupported{
                connection.videoOrientation = currentVideoOrientation()
                connection.videoOrientation = .portrait
                print("Current Orientation:\(connection.videoOrientation)")
            }
            
            if let connect = connection ,  connect.isVideoStabilizationSupported{
                connection?.preferredVideoStabilizationMode = AVCaptureVideoStabilizationMode.auto
            }
            
            if let connect = connection {
                if let output = movieOutput , output.availableVideoCodecTypes.contains(.h264) {
                    // Use the H.264 codec to encode the video.
                    movieOutput?.setOutputSettings([AVVideoCodecKey: AVVideoCodecType.h264], for: connect)
                }
            }
            
            if let activeinput = activeInput {
                let device = activeinput.device
            if (device.isSmoothAutoFocusSupported) {
                
                do {
                    try device.lockForConfiguration()
                    device.isSmoothAutoFocusEnabled = false
                    device.unlockForConfiguration()
                } catch {
                    print("Error setting configuration: \(error)")
                }
            }
                outputURL = tempURL()
                
                self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
                
                btnRecord.isSelected = true
                isCameraRotate = false
                movieOutput?.startRecording(to: outputURL, recordingDelegate: self)
            }else{
                print("Camera permission is not granted")
            }
            //EDIT2: And I forgot this
        }
        else {
            self.totalTime = 4
            self.lblTimer.text = "00:05"
            stopRecording()
            stop()
            btnRecord.isSelected = false
        }
    }
    
    //MARK:-  Change for rotate camera Action Methods
    @IBAction func clickOnCameraRotateButton(_ sender: UIButton) {
        if btnRecord.isSelected{
            btnRecord.isSelected = false
            self.timer?.invalidate()
            self.totalTime = 4
            self.lblTimer.text = "00:05"
        }
        isCameraRotate = true
        stopRecording()
        //Change camera source
        //Remove existing input
        guard var currentCameraInput: AVCaptureInput = captureSession?.inputs.first else {
            return
        }
        
        //Indicate that some changes will be made to the session
        captureSession?.beginConfiguration()
        captureSession?.sessionPreset = AVCaptureSession.Preset.medium
        
        if let inputs = captureSession?.inputs as? [AVCaptureDeviceInput]{
            for input in inputs{
                if input.device.hasMediaType(.video){
                    currentCameraInput = input
                    captureSession?.removeInput(input)
                }
            }
        }
        
        //Get new input
        var newCamera: AVCaptureDevice! = nil
        if let input = currentCameraInput as? AVCaptureDeviceInput {
            if (input.device.position == .back) {
                newCamera = cameraWithPosition(position: .front)
            } else {
                newCamera = cameraWithPosition(position: .back)
            }
        }
        
        //Add input to session
        var err: NSError?
        var newVideoInput: AVCaptureDeviceInput!
        do {
            newVideoInput = try AVCaptureDeviceInput(device: newCamera)
        } catch let err1 as NSError {
            err = err1
            newVideoInput = nil
        }
        
        if newVideoInput == nil || err != nil {
            print("Error creating capture device input: \(err?.localizedDescription ?? "")")
        } else if let session = captureSession , session.canAddInput(newVideoInput) {
            captureSession?.addInput(newVideoInput)
            self.activeInput = newVideoInput
        }
        
        //Commit all the configuration changes at once
        captureSession?.commitConfiguration()
        //startRecording()
    }

       // Find a camera with the specified AVCaptureDevicePosition, returning nil if one is not found
       func cameraWithPosition(position: AVCaptureDevice.Position) -> AVCaptureDevice? {
           let discoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: AVMediaType.video, position: .unspecified)
           for device in discoverySession.devices {
               if device.position == position {
                   return device
               }
           }

           return nil
       }
    
    override func onClickDismiss(_ sender: Any ) {
        self.onDissMiss?()
        if let Vcs = self.navigationController?.viewControllers {
            for vc in Vcs{
                if let cameraPreview = vc as? CameraPreviewVC{
                    print("exit camera Screen")
                    self.delay(0) {
                        cameraPreview.autoBackBtnPress()
                    }
                    break
                }
            }
        }
        resetSession()
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func resetSession(isVideoAdded:Bool = false){
        self.stopSession()
        if isVideoAdded{
            captureSession = nil
            movieOutput = nil
            previewLayer = nil
            defaultVideoDevice = nil
            activeInput = nil
        }
        print("Stop Session")
    }
    
    func stopRecording() {
        if let output = movieOutput, output.isRecording == true {
            print("stopRecording is true")
            movieOutput?.stopRecording()
            btnRecord.isSelected = false
        }else{
            print("stopRecording is false")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.timer?.invalidate()
    }
    
    @objc func record() {
        cameraPermissionForRecordVideo()
    }
    
    func cameraPermissionForRecordVideo(){
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized && isAudioAuthorized(){
            //already authorized
            DispatchQueue.main.async {
                self.startRecording()
            }
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                DispatchQueue.main.async {
                    if granted {
                        //access allowed
                        AVCaptureDevice.requestAccess(for: .audio, completionHandler: { (granted: Bool) in
                            DispatchQueue.main.async {
                                if granted {
                                    //access allowed
                                    print("audio access")
                                    self.startRecording()
                                }else {
                                    print("Access denied")
                                    // Create Alert
                                    self.cameraSettingPopup()
                                }
                            }
                        })
                       // self.startRecording()
                    }else {
                        print("Access denied")
                        // Create Alert
                        self.cameraSettingPopup()
                    }
                }
            })
        }
    }
    
    func cameraSettingPopup(){
        let alert = UIAlertController(title: "Camera", message: "Camera and Microphone access is absolutely necessary to use Frenzy App.", preferredStyle: .alert)

        // Add "OK" Button to alert, pressing it will bring you to the settings app
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }))
        // Show the alert with animation
        self.present(alert, animated: true)
    }
    
   /* func tempURL(postFix:String = "") -> URL? {
        let directory = NSTemporaryDirectory() as NSString
        
        if directory != "" {
            let path = directory.appendingPathComponent(NSUUID().uuidString + postFix + ".mp4")
            return URL(fileURLWithPath: path)
        }
        
        return nil
    } */
    
    func tempURL() -> URL? {
            let directory = NSTemporaryDirectory() as NSString
            if directory != "" {
                let path = directory.appendingPathComponent(NSUUID().uuidString + ".mp4")
                return URL(fileURLWithPath: path)
            }
            return nil
    }
    
    var isRecodingProperly = false
   
    @objc func stop() {
        self.timer?.invalidate()
        if let output =  movieOutput , output.isRecording == true {
            self.isRecodingProperly = true
            print(" stop isRecodingProperly \(isRecodingProperly)")
            movieOutput?.stopRecording()
        }else{
             self.isRecodingProperly = false
            print(" stop isRecodingProperly \(isRecodingProperly)")
        }
    }
    
    @objc func updateTimer() {
        self.timeFormatted(self.totalTime)
        if totalTime != 0 {
            totalTime -= 1
           // self.btnRecord.setProgress(CGFloat((Double(4 - self.totalTime)/4.0)))
        }else{
            print("updateTimer Stop Recording")
            self.stopRecording()
            self.stop()
            self.totalTime = 4
        }
    }
    
    func timeFormatted(_ totalSeconds: Int) {
        self.redView.BlinkIn()
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        UIView.transition(with: self.lblTimer,
                          duration: 0.25,
                          options: .curveEaseIn,
                          animations: { [weak self] in
                            if(seconds < 10){
                                self?.lblTimer.text = "0\(seconds)"
                            }else{
                                self?.lblTimer.text = "\(seconds)"
                            }
                            if minutes < 10 {
                                self?.lblTimer.text = "0\(minutes):\(self?.lblTimer.text ?? "")"
                            }else{
                                self?.lblTimer.text = "\(minutes):\(self?.lblTimer.text ?? "")"
                            }
            }, completion: nil)
        
    }
    
    func currentVideoOrientation() -> AVCaptureVideoOrientation {
        var orientation: AVCaptureVideoOrientation = .portrait
        
        switch UIDevice.current.orientation {
        case .portrait:
            orientation = AVCaptureVideoOrientation.portrait
        case .landscapeRight:
            orientation = AVCaptureVideoOrientation.landscapeLeft
        case .portraitUpsideDown:
            orientation = AVCaptureVideoOrientation.portraitUpsideDown
        default:
            orientation = AVCaptureVideoOrientation.portrait
        }
        return orientation
    }
    
    func capture(_ captureOutput: AVCaptureFileOutput!, didStartRecordingToOutputFileAt fileURL: URL!, fromConnections connections: [Any]!) {
   
    }
    
    func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        if !self.isRecodingProperly{
            return
        }
        print("outPutFile \(outputFileURL.absoluteString)")
        guard let image = generateThumbnail(path: outputFileURL) else { return }
        let size = image.size //?? CGSize.init(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
                print("outPutFileThum \(image.size as Any)")
        
                let ratio = size.width > size.height ? (size.width/size.height) : (size.height/size.width)
                let avAsset = AVURLAsset(url: outputFileURL, options: nil)
                print("avAsset Record length: \(Int(avAsset.duration.seconds))")
                let length = Int(avAsset.duration.seconds) //5 - self.totalTime
        
            print("IsCamera rotate:\(isCameraRotate)")
         
            if !isCameraRotate{
            
                let vc = CameraPreviewVC.instantiate(fromAppStoryboard: .Movement)
                vc.ratio = ratio
                vc.length = length
                vc.image = image
                vc.outputFileURL = outputFileURL
                vc.videoUrl = outputFileURL
                vc.momentId = self.momentId!
             
                vc.videoAdded = { itemModel in
                    print("video Added")
                    self.videoAdded?(itemModel)
                    self.resetSession(isVideoAdded: true)
                    self.delay(0.0, completion: {
                        self.navigationController?.popViewController(animated: false)
                    })
                }
                
                vc.videoUploaded = {
                    print("video uploaded")
                    self.resetSession()
                    self.delay(0, completion: {
                        self.navigationController?.popViewController(animated: false)
                    })
                }
            
                vc.onBackPressed = {
                    print("back pressed")
                    self.resetSession()
                    self.delay(0, completion: {
                        self.onDissMiss?()
                        self.navigationController?.popViewController(animated: false)
                    })
                }
                self.totalTime = 4
                self.lblTimer.text = "00:05"
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                isCameraRotate = false
            }
    }
    
    func generateThumbnail(path: URL) -> UIImage? {
        do {
            let asset = AVURLAsset(url: path, options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            return thumbnail
        } catch let error {
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
        }
    }
    
    //MARK:- Camera Session/
    func startSession() {
        
        if let session = captureSession, !session.isRunning {
            videoQueue().async {
                self.captureSession?.startRunning()
            }
        }
    }
    
    func stopSession() {
        if let session = captureSession, session.isRunning {
            videoQueue().async {
                self.captureSession?.stopRunning()
            }
        }
    }
    
    
    func videoQueue() -> DispatchQueue {
        return DispatchQueue.main
    }
    
    func setupSession() -> Bool {
        captureSession = AVCaptureSession()
        movieOutput = AVCaptureMovieFileOutput()
        captureSession?.sessionPreset = AVCaptureSession.Preset.medium
      
        if #available(iOS 13.0, *) {
              let _ = try? AVAudioSession.sharedInstance().setAllowHapticsAndSystemSoundsDuringRecording(true)
        }
        // Setup Camera
        let camera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front)! //(for: AVMediaType.video)!
        
        do {
            self.defaultVideoDevice = camera
            
            let input = try AVCaptureDeviceInput(device: camera)
            
            if let session = captureSession , session.canAddInput(input) {
                captureSession?.addInput(input)
                self.activeInput = input
            }
        } catch {
            print("Error setting device video input: \(error)")
            return false
        }
        
        // Setup Microphone
        let microphone = AVCaptureDevice.default(for: AVMediaType.audio)!
        
        do {
            let micInput = try AVCaptureDeviceInput(device: microphone)
            if let session = captureSession , session.canAddInput(micInput) {
                captureSession?.addInput(micInput)
            }
        } catch {
            print("Error setting device audio input: \(error)")
            return false
        }
        // Movie output
        if let session = captureSession , let output = movieOutput, session.canAddOutput(output) {
            captureSession?.addOutput(output)
        }
        
        return true
    }
    
    func setupPreview() {
        // Configure previewLayer
        guard  let captureSession = captureSession else {
            return
        }
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer?.frame = cameraView.bounds
        previewLayer?.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        previewLayer?.videoGravity = AVLayerVideoGravity.resizeAspect//resizeAspectFill
        cameraView.layer.addSublayer(previewLayer!)
    }
}

@objc public enum RecordButtonNewState : Int {
    case recording, idle, hidden;
}

