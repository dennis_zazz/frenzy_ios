//
//  AnimationTextVC.swift
//  Frenzy
//
//  Created by Jessie on 4/12/20.
//  Copyright © 2020 Jhony. All rights reserved.
//

import UIKit

class AnimationTextVC: BaseViewController {
  
    @IBOutlet weak var cheerView: CheerView!
    @IBOutlet weak var logo: UIImageView!
    static let identifier = "AnimationTextVC"
    var seatNumber : Int = 0
    var bg_Color = FrenzyColor.blackKindColor
    var text_Color = FrenzyColor.frozyKindColor
    var animateText = ""
    var time  = ""
    var event_id = 0
    var timer: Timer?
    var momentAnimationTime = 1.0
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var crossView: UIView!
    @IBOutlet weak var tf_seat: UITextField!
    var mainParentView : UIView!
    @IBOutlet weak var leading: NSLayoutConstraint!
    @IBOutlet weak var widthConstratins: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
       self.contentView.isHidden = false
       self.contentView.backgroundColor = bg_Color
       self.cheerView.config.particle = .confetti(allowedShapes: [Particle.ConfettiShape.circle])
       self.cheerView.startLow()
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(action), userInfo: nil, repeats: true)
        self.view.bringSubviewToFront(self.crossView)
        IOSocket.sharedInstance.stopMoment(completion: { result in
            if isLogOutCalled {
                return
             }
            if result.count > 0 {
                if self.event_id == result.first!["event_id"] as? Int ?? 0{
                    self.onClickDismiss(UIButton())
                }
            }
        })
    }
    @objc func action () {
        if time.isEmpty{
            self.startMoment()
        }else{
            let actualDate = time.toDate(dateFormat: "yyyy-MM-dd HH:mm:ss").toLocalTime().toString(dateFormat: "yyyy-MM-dd HH:mm")
            let currentData = Date().toLocalTime().toString(dateFormat: "yyyy-MM-dd HH:mm")
            print("\(actualDate)  ==  \(currentData)")
            if  actualDate == currentData{
                timer?.invalidate()
                self.cheerView.isHidden = true
                self.logo.isHidden = true
                self.startMoment()
            }
        }
    }
    
    func startMoment() {
        var delay = Double(Double(self.seatNumber))
        if delay < 1 {
            delay = momentAnimationTime
        }
        print("\(delay)")
        self.delay(delay) {
            self.restartAnimation(text: self.animateText.uppercased())
        }
    }
    @IBAction func onClickSubmitSeat(_ sender: Any) {
        if tf_seat.text!.isEmpty{
            self.showErrorToast(title: "Seat Number Missing!", description: "Enter your seat number to continue!") { (isPress) in
            }
        }else{
            seatNumber = Int(self.tf_seat.text!)!
            self.contentView.isHidden = false
        }
    }
    
    func restartAnimation(text : String) {
        let screenWidth = self.view.frame.size.width
        let secreenHeight = self.view.frame.size.height
        let mainParentView_X = screenWidth
        let mainParentView_width = screenWidth*CGFloat(text.count)
        mainParentView = UIView.init(frame: CGRect.init(x: mainParentView_X, y: 0, width: mainParentView_width, height: secreenHeight))
        mainParentView.backgroundColor = .clear
        self.view.addSubview(mainParentView)
        for index in 0..<text.count{
            let label_X = screenWidth*(CGFloat(index))
            let label = UILabel.init(frame: CGRect.init(x: label_X, y: 0, width: screenWidth, height: secreenHeight))
            label.text = String(text[index])
            label.textColor = self.text_Color
            label.textAlignment = .center
            label.font = UIFont.init(name: FontFamilyName.kNunitoBold, size: CGFloat.init(316))
            mainParentView.addSubview(label)
        }
        self.view.bringSubviewToFront(self.crossView)
        self.startAnimation(width: screenWidth, text: text)
    }
    
    func startAnimation(width : CGFloat , text : String) {
         let mainParentView_width = width*CGFloat(text.count)
         UIView.animate(withDuration: momentAnimationTime, delay: 0, options: .curveLinear, animations: {
            self.mainParentView.frame.origin.x -= width
        }, completion: {(isComplete) in
            if(self.mainParentView.frame.origin.x < -mainParentView_width ){
                self.restartAnimation(text: text)
            }else{
               self.startAnimation(width: width, text: text)
            }
         })
    }
}
