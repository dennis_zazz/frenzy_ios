//
//  CommentHeaderTblCell.swift
//  Frenzy
//
//  Created by Nandini Yadav on 16/07/21.
//  Copyright © 2021 Jhony. All rights reserved.
//

import UIKit

class CommentHeaderTblCell: UITableViewCell {
    static let identifier = "CommentHeaderTblCell"

    @IBOutlet weak var commentLbl: UILabel!
    @IBOutlet weak var commentButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
