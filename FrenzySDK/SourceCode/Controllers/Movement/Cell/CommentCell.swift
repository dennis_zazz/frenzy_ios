//
//  CommentCell.swift
//  Frenzy
//
//  Created by CodingPixel on 19/03/2020.
//  Copyright © 2020 Jhony. All rights reserved.
//

import UIKit
import ActiveLabel

class CommentCell: UITableViewCell {
    
    static let identifier = "CommentCell"
    
    @IBOutlet weak var ivProfileImage: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblComment: ActiveLabel!
    @IBOutlet weak var editedLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var commentGifImage: UIImageView!
    
    var comment: Comment?
    var reloadRowAfterGifLoadClousure: ((_ isGifurlInValid:Bool)->Void) = { _ in}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblComment.enabledTypes = [ .url]
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func  setAttributedTextForLabel(mainString : String , attributedStringsArray : [String] , lbl : UILabel , color : UIColor, attFont:UIFont) {
       
        let attributedString1 = NSMutableAttributedString(string: mainString)
       
        for objStr in attributedStringsArray {
            let range1 = (mainString as NSString).range(of: objStr)
            let attribute_font = [NSAttributedString.Key.font: attFont]
            attributedString1.addAttributes(attribute_font, range:  range1)
            attributedString1.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range1)
        }
        lbl.attributedText = attributedString1
    }
    
    func configure() {
        guard let comment = comment else {
            return
        }
       
        ivProfileImage.kf.setImage(with: URL(string: (comment.profileImage ?? "").trimmingCharacters(in: .whitespaces)), placeholder:  UIImage(named: "profile_placeholder", in: Bundle(for: CommentCell.self), with: nil))
        lblName.text = comment.name ?? ""
        lblTime.text = comment.postTime ?? ""
        
        let imgUrls = checkForUrls(text: comment.comment ?? "")
        
        var editedComment = comment.comment ?? ""
        self.commentGifImage.isHidden = true
        
        if imgUrls.count > 0 , let firstGifUrl = imgUrls.first{
            
            if firstGifUrl.lastPathComponent.contains(".gif"){
                commentGifImage.kf.setImage(with:firstGifUrl , placeholder: UIImage(named: "placeholder", in: Bundle(for: CommentCell.self), with: nil))
                if commentGifImage.image == UIImage(named: "placeholder", in: Bundle(for: CommentCell.self), with: nil){
                    self.commentGifImage.isHidden = true
                    self.setCommnentData(commentText: editedComment, comment: comment)
                }else{
                    self.commentGifImage.isHidden = false
                    editedComment = editedComment.replacingOccurrences(of: firstGifUrl.absoluteString, with: "")
                    self.setCommnentData(commentText: editedComment, comment: comment)
                }
            }else{
                commentGifImage.isHidden = true
            }
        }else{
            commentGifImage.isHidden = true
        }
        setCommnentData(commentText: editedComment, comment: comment)
    }
    
    func setCommnentData(commentText:String , comment:Comment){
        
        if comment.isUpdate == 1 {
            self.setAttributedTextForLabel(mainString: (commentText) + " (edited)", attributedStringsArray: ["(edited)"], lbl: lblComment, color: UIColor.init(hexColor: "3F1551").withAlphaComponent(0.5), attFont: lblComment.font!)
        }else{
            lblComment.text = commentText// (comment.comment ?? "")
        }
        if (comment.isUpdate ?? 0) == 0 {
            editedLabelHeight.constant = 0
        } else {
            editedLabelHeight.constant = 0
        }
    }
    
    func checkForUrls(text: String) -> [URL] {
        let types: NSTextCheckingResult.CheckingType = .link
        do {
            let detector = try NSDataDetector(types: types.rawValue)
            let matches = detector.matches(in: text, options: .reportCompletion, range: NSMakeRange(0, text.count))
            return matches.compactMap({$0.url})
        } catch let error {
            debugPrint(error.localizedDescription)
        }
        return []
    }
    
}
