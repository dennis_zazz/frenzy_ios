//
//  VideoPlayerClass.swift
//  Frenzy
//
//  Created by CP on 1/22/20.
//  Copyright © 2020 Jhony. All rights reserved.
//

import Foundation
import UIKit

class VGVerticalViewController: UIViewController {
    static let identifier = "VGVerticalViewController"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: "\(APIConstants.BasePath)/Frenzy_HowTo_v03_final.mp4")
        //"https://web.ohheyfrenzy.com/public/Frenzy_HowTo_v03_final.mp4")
        
        if url != nil {
            //player = VGPlayer(URL: url!)
        }
//        player?.delegate = self
//        view.addSubview((player?.displayView)!)
//        player?.backgroundMode = .autoPlayAndPaused//.suspend//proceed//
//        player?.play()
//        player?.displayView.delegate = self
//        player?.displayView.titleLabel.text = "How Frenzy Works"
//        player?.displayView.snp.makeConstraints { [weak self] (make) in
//            guard let strongSelf = self else { return }
//            make.edges.equalTo(strongSelf.view)
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
}
