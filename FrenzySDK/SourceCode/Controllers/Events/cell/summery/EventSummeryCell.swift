
import UIKit

class EventSummeryCell: UITableViewCell {
    
    static let identifier = "EventSummeryCell"
    @IBOutlet weak var viewLeftSide:UIView!
    @IBOutlet weak var viewRightSide:UIView!
    @IBOutlet weak var ivLeftCenter:UIImageView!
    @IBOutlet weak var ivRigtCenter:UIImageView!
    //
    @IBOutlet weak var ivRigtUser:UIImageView!
    @IBOutlet weak var lblRigtTitle:UILabel!
    @IBOutlet weak var lblRigtName:UILabel!
    @IBOutlet weak var ivLeftUser:UIImageView!
    @IBOutlet weak var lblLeftTitle:UILabel!
    @IBOutlet weak var lblLeftName:UILabel!
    //
    @IBOutlet weak var lblTimeRight: UILabel!
    
    @IBOutlet weak var lblTimeleft: UILabel!
    var delighter: Delighter?
    var movement: Movement?
    var isLeft = false
    var baseVC: BaseViewController?
    var eventId = 0
    var eventBottomColor : String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure() {
        guard let delight = delighter else {
            return
        }
        if isLeft {
            self.ivLeftCenter.image = UIImage.init(named: "ic_left_summery", in: Bundle(for: EventSummeryCell.self) , with:nil)
            self.viewLeftSide.isHidden = false
            self.ivLeftCenter.isHidden = false
            self.viewRightSide.isHidden = true
            self.ivRigtCenter.isHidden = true
            let titleLbl = "<span style=\"color:#ffffff\">\(delight.title)</span>"
            let subTitle = "<span style=\"color:#ffffff\">\(delight.description)</span>"
            self.lblLeftTitle.attributedText = titleLbl.htmlToAttributedString
            self.lblLeftName.attributedText = subTitle.htmlToAttributedString
            self.ivLeftUser.kf.setImage(with: URL.init(string: delight.image ?? ""), placeholder: UIImage(named: "placeholder", in: Bundle(for: EventSummeryCell.self), with: nil))
            self.lblTimeleft.text = delight.time!.UTCToLocal(inputFormate: "yyyy-MM-dd HH:mm:ss", outputFormate: "hh:mm a")
        } else {
            self.ivRigtCenter.image = UIImage(named: "ic_right_summery" , in: Bundle(for: EventSummeryCell.self) , with:nil)
            self.viewLeftSide.isHidden = true
            self.ivLeftCenter.isHidden = true
            self.viewRightSide.isHidden = false
            self.ivRigtCenter.isHidden = false
            let titleLbl = "<span style=\"color:#ffffff\">\(delight.title)</span>"
            let subTitle = "<span style=\"color:#ffffff\">\(delight.description)</span>"
            self.lblRigtTitle.attributedText = titleLbl.htmlToAttributedString
            self.lblRigtName.attributedText = subTitle.htmlToAttributedString
            self.ivRigtUser.kf.setImage(with: URL.init(string: delight.image ?? ""), placeholder: UIImage(named: "placeholder", in: Bundle(for: EventSummeryCell.self), with: nil))
            self.lblTimeRight.text = delight.time!.UTCToLocal(inputFormate: "yyyy-MM-dd HH:mm:ss", outputFormate: "hh:mm a")
        }
        self.lblLeftTitle.lineBreakMode = .byTruncatingTail
        self.lblRigtTitle.lineBreakMode = .byTruncatingTail
        self.lblLeftName.isHidden = false
        self.lblRigtName.isHidden = false
        
    }
    
    func configureMovement() {
        guard let moment = movement else {
            return
        }
        self.lblLeftName.isHidden = true
        self.lblRigtName.isHidden = true
         self.lblRigtName.text = nil
        self.lblLeftName.text = nil
        if isLeft {
            self.ivLeftCenter.image = UIImage(named: "ic_moment_left", in: Bundle(for: EventSummeryCell.self) , with:nil)
            self.viewLeftSide.isHidden = false
            self.ivLeftCenter.isHidden = false
            self.viewRightSide.isHidden = true
            self.ivRigtCenter.isHidden = true
            let titleLbl = "<span style=\"color:#ffffff\">\(moment.title)</span>"
            self.lblLeftTitle.attributedText = titleLbl.htmlToAttributedString
            self.ivLeftUser.kf.setImage(with: URL.init(string: moment.image ), placeholder: UIImage(named: "placeholder", in: Bundle(for: EventSummeryCell.self), with:nil))
            self.lblTimeleft.text = moment.date.UTCToLocal(inputFormate: "yyyy-MM-dd HH:mm:ss", outputFormate: "hh:mm a")
        } else {
            self.ivRigtCenter.image = UIImage.init(named: "ic_moment_right", in: Bundle(for: EventSummeryCell.self) , with:nil)
            self.viewLeftSide.isHidden = true
            self.ivLeftCenter.isHidden = true
            self.viewRightSide.isHidden = false
            self.ivRigtCenter.isHidden = false
            let titleLbl = "<span style=\"color:#ffffff\">\(moment.title)</span>"

            self.lblRigtTitle.attributedText = titleLbl.htmlToAttributedString
            self.lblTimeRight.text = moment.date.UTCToLocal(inputFormate: "yyyy-MM-dd HH:mm:ss", outputFormate: "hh:mm a")
            self.ivRigtUser.kf.setImage(with: URL.init(string: moment.image ), placeholder: UIImage(named: "placeholder", in: Bundle(for: EventSummeryCell.self), with:nil))
        }
        self.lblLeftTitle.lineBreakMode = .byTruncatingTail
        self.lblRigtTitle.lineBreakMode = .byTruncatingTail
    }
    
    @IBAction func onClickCell(_ sender: Any) {
        guard let base = baseVC else{
            return
        }
        if let dlt  = delighter {
             base.openDelighterVC(with: dlt , eventID: "\(eventId)" , bottomColor: eventBottomColor)
       
        }else if let mvmnt = movement{
            
            let vc = MovemenVideosVC.instantiate(fromAppStoryboard: .Movement)
            vc.momentId = mvmnt.id
            vc.eventId = eventId
            base.navigationController?.pushViewController(vc, animated: true)
        }
       
    }
}
