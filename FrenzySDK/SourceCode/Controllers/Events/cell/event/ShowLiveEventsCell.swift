//
//  ShowLiveEventsCell.swift
//  Frenzy
//
//  Created by Jessie on 4/15/20.
//  Copyright © 2020 Jhony. All rights reserved.
//

import UIKit
protocol LiveEventDelegate {
    func onUpdateLiveEvent(isShowLiveEvent : Bool)
}
class ShowLiveEventsCell: UITableViewCell {
    static let identifier = "ShowLiveEventsCell"
    var livedelegate : LiveEventDelegate?
    @IBOutlet weak var liveSwitch: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onUpdatSwitch(_ sender: Any) {
        livedelegate?.onUpdateLiveEvent(isShowLiveEvent: liveSwitch.isOn)
    }
    
}
