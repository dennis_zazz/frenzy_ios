

import UIKit

class EventsCell: UITableViewCell {
    static let identifier = "EventsCell"
    var baseVC : BaseViewController?
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var backgroundViewWidth: NSLayoutConstraint!
    
    @IBOutlet weak var lblTeam: UILabel!
    @IBOutlet weak var bgView: UIView!
    private var correction: CGFloat = 0
    var onDragOpenDetail:(() -> Void)?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.addDragGesture()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setliveEventView(isLive : Bool,item : EventItem)  {
        if isLive {
            self.lblTitle.textColor = .white
            self.lblTeam.textColor = .white
            dateView.backgroundColor = FrenzyColor.redColor//
            bgView.backgroundColor = FrenzyColor.frozyKindColor
            if(item.eventCategory == .EVENT){
                self.baseVC?.setAttributedTextForLabel(mainString: "\(item.eventName ?? "") \(item.eventPlace ?? "")", attributedStringsArray: [item.eventPlace ?? ""], lbl: self.lblTitle, color: FrenzyColor.grayKindColor, attFont: FrenzyFont.kNunitoRegular(8.9).font())
                self.lblTeam.text = ""
                self.baseVC?.setAttributedTextForLabel(mainString: "\(item.time ?? "")   \(item.place ?? "")   \(item.location ?? "")", attributedStringsArray: [item.place ?? ""], lbl: self.lblDetail, color: FrenzyColor.white, attFont: FrenzyFont.kNunitoRegular(8.9).font())
                self.lblDate.text = item.date
                self.lblDay.text = item.day
            }else{
                self.lblTeam.text = ""
                self.baseVC?.setAttributedTextForLabel(mainString:"\(item.eventName ?? "") \(item.eventPlace ?? "")", attributedStringsArray: ["vs"], lbl: self.lblTitle, color: FrenzyColor.lightGray, attFont: FrenzyFont.kNunitoRegular(8.9).font())
//                 self.baseVC?.setAttributedTextForLabel(mainString:"\(item.teamOne ?? "") vs \(item.teamTwo ?? "")", attributedStringsArray: ["vs"], lbl: self.lblTeam, color: FrenzyColor.lightGray, attFont: FrenzyFont.kNunitoRegular(8.9).font())
                self.baseVC?.setAttributedTextForLabel(mainString: "\(item.time ?? "")   \(item.place ?? "")   \(item.location ?? "")", attributedStringsArray: [item.place ?? ""], lbl: self.lblDetail, color: FrenzyColor.white, attFont: FrenzyFont.kNunitoRegular(8.9).font())
                self.lblDate.text = item.date
                self.lblDay.text = item.day
            }
        }else{
            self.lblTitle.textColor = FrenzyColor.frozyKindColor
            self.lblTeam.textColor = FrenzyColor.frozyKindColor
            bgView.backgroundColor = FrenzyColor.white
            if(item.eventCategory == .EVENT){
                dateView.backgroundColor = FrenzyColor.frozyKindColor
                self.baseVC?.setAttributedTextForLabel(mainString: "\(item.eventName ?? "") \(item.eventPlace ?? "")", attributedStringsArray: [item.eventPlace ?? ""], lbl: self.lblTitle, color: FrenzyColor.grayKindColor, attFont: FrenzyFont.kNunitoRegular(8.9).font())
                self.lblTeam.text = ""
                self.baseVC?.setAttributedTextForLabel(mainString: "\(item.time ?? "")   \(item.place ?? "")   \(item.location ?? "")", attributedStringsArray: [item.place ?? ""], lbl: self.lblDetail, color: FrenzyColor.purpleColor, attFont: FrenzyFont.kNunitoRegular(8.9).font())
                self.lblDate.text = item.date
                self.lblDay.text = item.day
            }else{
                self.lblTeam.text = ""
                self.baseVC?.setAttributedTextForLabel(mainString: "\(item.eventName ?? "") \(item.eventPlace ?? "")", attributedStringsArray: ["vs"], lbl: self.lblTitle, color: FrenzyColor.grayKindColor, attFont: FrenzyFont.kNunitoRegular(8.9).font())
//                 self.baseVC?.setAttributedTextForLabel(mainString:"\(item.teamOne ?? "") vs \(item.teamTwo ?? "")", attributedStringsArray: ["vs"], lbl: self.lblTeam, color: FrenzyColor.grayKindColor, attFont: FrenzyFont.kNunitoRegular(8.9).font())
                self.baseVC?.setAttributedTextForLabel(mainString: "\(item.time ?? "")   \(item.place ?? "")   \(item.location ?? "")", attributedStringsArray: [item.place ?? ""], lbl: self.lblDetail, color: FrenzyColor.purpleColor, attFont: FrenzyFont.kNunitoRegular(8.9).font())
                self.lblDate.text = item.date
                self.lblDay.text = item.day
                dateView.backgroundColor = FrenzyColor.greenColor//
            }
        }
    }
    func configure(item : EventItem) {
        if(item.eventCategory == .EVENT){
            self.lblTeam.text = ""
            dateView.backgroundColor = FrenzyColor.frozyKindColor
            self.baseVC?.setAttributedTextForLabel(mainString: "\(item.eventName ?? "") \(item.eventPlace ?? "")", attributedStringsArray: [item.eventPlace ?? ""], lbl: self.lblTitle, color: FrenzyColor.grayKindColor, attFont: FrenzyFont.kNunitoRegular(8.9).font())
            self.baseVC?.setAttributedTextForLabel(mainString: "\(item.time ?? "")   \(item.place ?? "")   \(item.location ?? "")", attributedStringsArray: [item.place ?? ""], lbl: self.lblDetail, color: FrenzyColor.purpleColor, attFont: FrenzyFont.kNunitoRegular(8.9).font())
            self.lblDate.text = item.date
            self.lblDay.text = item.day
        }else{
           self.baseVC?.setAttributedTextForLabel(mainString: "\(item.eventName ?? "") \(item.eventPlace ?? "")", attributedStringsArray: [item.eventPlace ?? ""], lbl: self.lblTitle, color: FrenzyColor.grayKindColor, attFont: FrenzyFont.kNunitoRegular(8.9).font())
            self.lblTeam.text = ""
//            self.baseVC?.setAttributedTextForLabel(mainString: "\(item.teamOne ?? "") vs \(item.teamTwo ?? "")  ", attributedStringsArray: ["vs"], lbl: self.lblTeam, color: FrenzyColor.grayKindColor, attFont: FrenzyFont.kNunitoRegular(8.9).font())
            self.baseVC?.setAttributedTextForLabel(mainString: "\(item.time ?? "")   \(item.place ?? "")   \(item.location ?? "")", attributedStringsArray: [item.place ?? ""], lbl: self.lblDetail, color: FrenzyColor.purpleColor, attFont: FrenzyFont.kNunitoRegular(8.9).font())
            self.lblDate.text = item.date
            self.lblDay.text = item.day
            dateView.backgroundColor = FrenzyColor.greenColor//
        }
    }
    
}

// MARK: Gestuer Relate Code
extension EventsCell {
    
    private func addDragGesture() {
        let drag = UIPanGestureRecognizer(target: self, action: #selector(didPan))
        dateView.addGestureRecognizer(drag)
    }
    
    @objc private func didPan(panGesture: UIPanGestureRecognizer) {
        switch panGesture.state {
        case .cancelled, .ended, .failed:
            let location = panGesture.location(in: self)
            let newWidth = location.x - correction
            let actualCellWidth = self.contentView.frame.width
            let percentage = (newWidth / actualCellWidth) * 100
            print("Width : \(newWidth) , MainCellWidth : \(actualCellWidth), Percentage : \(percentage)")
            if(percentage > 70){
                self.onDragOpenDetail?()
            }
            moveToNearestPoint(basedOn: panGesture, velocity: panGesture.velocity(in: dateView))
        case .began:
            correction = panGesture.location(in: dateView).x - dateView.frame.width/2
        case .changed:
            let location = panGesture.location(in: self)
            let newWidth = location.x - correction
            if(newWidth < 75){
                self.backgroundViewWidth.constant = 75
            }else{
                self.backgroundViewWidth.constant = newWidth
            }
            
        case .possible: ()
        @unknown default: ()
        }
    }
    
    private func moveToNearestPoint(basedOn gesture: UIGestureRecognizer, velocity: CGPoint? = nil) {
        var location = gesture.location(in: self)
        if let velocity = velocity {
            let offset = velocity.x / 12
            location.x += offset
        }
        move(to: 0)
    }
    
    open func move(to index: Int) {
        let correctOffset = center(at: index)
        animate(to: correctOffset)
    }
    
    private func center(at index: Int) -> CGFloat {
        let xOffset = CGFloat(index) * dateView.frame.width + dateView.frame.width / 2
        return xOffset
    }
    
    private func animate(to position: CGFloat) {
        UIView.animate(withDuration: 0.2) {
            self.backgroundViewWidth.constant = 75
        }
    }
}
