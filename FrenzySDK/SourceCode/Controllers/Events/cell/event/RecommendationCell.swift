//
//  RecommendationCell.swift
//  Frenzy
//
//  Created by CP on 2/17/20.
//  Copyright © 2020 Jhony. All rights reserved.
//

import UIKit

class RecommendationCell: UITableViewCell {

    static let identifier = "RecommendationCell"
    
    @IBOutlet weak var uiColleectionView:UICollectionView!
    var baseVC:BaseViewController?
    var mData:[EventItem] = [EventItem]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setupCollectionView()
    }
    
    var callBackForSuggestion:((EventItem,Int) -> Void)?

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setupDataArray(mDataItems:[EventItem]){
        self.mData.removeAll()
        self.mData.append(contentsOf: mDataItems)
        self.uiColleectionView.reloadData()
    }
    
    func setupCollectionView(){
        self.uiColleectionView.dataSource = self
        self.uiColleectionView.delegate = self
        self.uiColleectionView.register(UINib.init(nibName: EventRecomendationCell.identifier, bundle: Bundle(for: EventRecomendationCell.self)), forCellWithReuseIdentifier: EventRecomendationCell.identifier)
        self.uiColleectionView.reloadData()
    }
    
}

extension RecommendationCell:UICollectionViewDataSource,UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.mData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:EventRecomendationCell.identifier, for: indexPath) as! EventRecomendationCell
        cell.baseVC = self.baseVC
        cell.configure(item: self.mData[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize.init(width: CGFloat(258), height: collectionView.frame.size.height)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.callBackForSuggestion?(self.mData[indexPath.row],indexPath.row)
    }
}
