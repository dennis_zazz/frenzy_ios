

import UIKit


class EventCountDownVC: BaseViewController {
    
    static let identifier = "EventCountDownVC"
    
    @IBOutlet weak var lblPersonCount: UILabel!
    @IBOutlet weak var lblSeconds: UILabel!
    @IBOutlet weak var lblMinutes: UILabel!
    @IBOutlet weak var lblHours: UILabel!
    //
    @IBOutlet weak var lblTeam1: UILabel!
    @IBOutlet weak var lblTeam2: UILabel!
    @IBOutlet weak var ivTeam1: UIImageView!
    @IBOutlet weak var ivTeam2: UIImageView!
    @IBOutlet weak var lblEventAddress: UILabel!
    //
    @IBOutlet weak var viewTeam: UIView!
    @IBOutlet weak var viewEvent: UIView!
    @IBOutlet weak var lblTeamEvent: UILabel!
    @IBOutlet weak var ivTeamEvent: UIImageView!
    @IBOutlet weak var ivSponsor: UIImageView!
    @IBOutlet weak var lblSponsorContent: UILabel!
    @IBOutlet weak var btnSponsor: UIButton!
    @IBOutlet weak var btnOpenSponsor: UIButton!
    //
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    //
    var refreshControl = FrenzyRefreshControl()
    var eventItem:EventItem = EventItem()
    var delighters = [Delighter]()
    //
    var timer: Timer?
    var totalTime = 3
    var progress:Float = 0
    var eventTitle : String = ""
    var hasViewAppeared = true      //call api repeatedly only when you are on the screen
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTable()
        startOtpTimer()
        self.configure(item:eventItem)
        self.tableView.addRefreshControll(actionTarget: self, action: #selector(refreshData))
        IOSocket.sharedInstance.connect()
        receiveSockets()
    }
    
    @objc func refreshData() {
        self.eventPostsGetCall()
    }
    
    deinit {

    }
    
    func receiveSockets(){
        
        IOSocket.sharedInstance.getEventEnd(completion: { result in
            print("result:\(result)")
            if result.count > 0 {
                var isDelete = ""
                if let value = result.first!["is_delete"] as? Int{
                    isDelete = "\(value)"
                }else if let value = result.first!["is_delete"] as? String{
                    isDelete = value
                }
                if isDelete == "1"{
                    if let nev = self.navigationController{
                        for controller in nev.viewControllers as Array {
                            if controller.isKind(of: HomeVC.self) {
                                nev.popToViewController(controller, animated: true)
                                break
                            }
                        }
                        
                    }else{
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        })
        
        IOSocket.sharedInstance.getPosts(completion: { result in
            if isLogOutCalled {
                return
            }
            if !isPostReceived {
                isPostReceived = true
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    isPostReceived = false
                })
                if result.count > 0 {
                    let eventIdArr = UserDefaults.standard.array(forKey: kEventIdKey) as? [Int] ?? [Int]()
                    let sectionIdArr = UserDefaults.standard.array(forKey: kSectionIdKey) as? [Int] ?? [Int]()
                  
                    print("Delighter Event ID Array:\(eventIdArr)")
                  
                    for eventId in eventIdArr {
                       
                        var eventID = ""
                      
                        if let id = result.first!["event_id"] as? String{
                            eventID = id
                        }
                        else if let id = result.first!["event_id"] as? Int{
                            eventID = "\(id)"
                        }
                        
                        if "\(eventId)" ==  eventID {
                            
                            let delighter = Delighter().initDelighter(from: result.first!)
                            let sectionsString = delighter.sections.components(separatedBy: ",")
                            let sections = sectionsString.compactMap { Int($0) }
                            
                            if isGlobalDemoEvent{
                                print("Demo delighter")
                                self.openDelighterVC(with: delighter, openBase64: false , eventID: eventID)
                                break
                            }else{
                                
                                if sections.count > 0 {
                                for sec in sections {
                                        print("Other Sections Array ")
                                        for mySec in sectionIdArr {
                                            print("Other Sections sectionIdArr ")
                                            if sec == mySec {
                                                print("Other Sections sec == mySec")
                                                self.eventPostsGetCall()
                                              //  self.openDelighterVC(with: delighter, openBase64: false , eventID: eventID)
                                                break
                                            }else{
                                                print("else Other Sections sec == mySec")
                                            }
                                        }
                                    }
                                }else{
                                    self.eventPostsGetCall()
                                  //  self.openDelighterVC(with: delighter, openBase64: false , eventID: eventID)
                                    break
                                }
                                }
                                break
                            }

                        }
                        
                    }
                }
        })
    }
    
    func updateBGcolor(with event: EventItem) {
        DispatchQueue.main.async {
            if event.topColor.contains("FFFFFF") || event.topColor.contains("ffffff"){
                self.topView.backgroundColor = .black
            }else{
                self.topView.backgroundColor = UIColor(hexColor: event.topColor)
            }
            self.bottomView.backgroundColor = UIColor(hexColor: event.bottomColor)
        }
    }
    
    func eventDetailGetCall(){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.eventDetailGet(eventId: eventItem.id ?? 0)), completion: { apiResponse in
            if self.hasViewAppeared {
                self.delay(3) {
                    self.eventDetailGetCall()
                }
            }
            self.tableView.endRefreshing()
            switch apiResponse {
            case .Failure(_):
//                self.lblPersonCount.text = String(self.eventItem.total_viewers ?? 0)
                break
            case .Success(let data):
                guard let result = data?.result as? EventItem else {
                    return
                }
                if let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? FrenzyCountCell {
                    cell.updateFrenzies(with: result.total_viewers ?? 0)
                }
                self.eventItem.total_viewers = result.total_viewers ?? 0
                if result.eventStatus == .ONGOING{
                    if let vcs = self.navigationController?.viewControllers {
                        let vc = EventMovementVC.instantiate(fromAppStoryboard: .home)
                        self.eventItem.event_status = EventStatus.ONGOING.rawValue
                        vc.eventItem = self.eventItem
                        vcs.first?.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                break
            }
        })
    }
    
    func configure(item:EventItem){
        configureDateAndLocation(of: item)
        updateBGcolor(with: item)
        DispatchQueue.main.async {
            self.tableView.pullAndRefresh()
        }
        self.eventDetailGetCall()
        switch item.eventCategory {
        case .MATCH:
            self.lblTeam1.text = item.teamOne
            self.lblTeam2.text = item.teamTwo
            self.ivTeam1.kf.setImage(with: URL.init(string: item.team1_logo ?? ""), placeholder: UIImage(named: "placeholder", in: Bundle(for: EventCountDownVC.self), with: nil))
            self.ivTeam2.kf.setImage(with: URL.init(string: item.team2_logo ?? ""), placeholder: UIImage(named: "placeholder", in: Bundle(for: EventCountDownVC.self), with: nil))
            self.viewTeam.isHidden = false
            self.viewEvent.isHidden = !self.viewTeam.isHidden
            break
        case .EVENT:
            self.lblTeamEvent.text = eventTitle//item.eventName
            self.ivTeamEvent.kf.setImage(with: URL.init(string: item.team1_logo ?? ""), placeholder: UIImage(named: "placeholder", in: Bundle(for: EventCountDownVC.self), with: nil))
            self.viewTeam.isHidden = true
            self.viewEvent.isHidden = !self.viewTeam.isHidden
            break
        case .none:
            break
        }
    }
    
    func configureDateAndLocation(of event: EventItem) {
        let date = event.event_timestamp?.getDateFormat(inputFormat: "yyyy-MM-dd HH:mm:ss", outPutFormat: "MM/dd/yyyy") ?? ""
        var completeAddress = ""
        if !(event.loc_title ?? "").isEmpty{
            completeAddress = event.loc_title!.trimmingCharacters(in: .whitespaces)
        }
        
        
        if !(event.loc_city ?? "").isEmpty {
            if completeAddress.isEmpty{
               completeAddress =  event.loc_city!.trimmingCharacters(in: .whitespaces)
            }else{
                 completeAddress = completeAddress + "," + event.loc_city!.trimmingCharacters(in: .whitespaces)
            }
            
        }
        
        if !(event.loc_state ?? "").isEmpty {
            if completeAddress.isEmpty{
                 completeAddress = event.loc_state!.trimmingCharacters(in: .whitespaces)
            }else{
                 completeAddress = completeAddress + "," + event.loc_state!.trimmingCharacters(in: .whitespaces)
            }
        }
        
        if completeAddress.isEmpty{
            lblEventAddress.text = date
        }else{
            lblEventAddress.text = date + ", " + completeAddress
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupBettryIconBlackOrWhite(isBlack: false)
        self.eventPostsGetCall()
        hasViewAppeared = true
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        self.timer?.invalidate()
        hasViewAppeared = false
    }
    
    
    private func startOtpTimer() {
        self.totalTime = eventItem.remainingEventSeconds ?? 0
        timeFormatted(totalTime)
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        self.timeFormatted(self.totalTime)
        if totalTime != 0 {
            totalTime -= 1
        } else {
            if let timer = self.timer {
                timer.invalidate()
                self.timer = nil
                if let vcs = self.navigationController?.viewControllers {
                    let vc = EventMovementVC.instantiate(fromAppStoryboard: .home)
                    self.eventItem.event_status = EventStatus.ONGOING.rawValue
                    vc.eventItem = self.eventItem
                    vcs.first?.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    func timeFormatted(_ totalSeconds: Int) {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        let hours: Int = (totalSeconds / 60) / 60
//        setTimeLabelSize(withRespectTo: hours)
        UIView.transition(with: self.lblSeconds,
                          duration: 0.25,
                          options: .curveEaseIn,
                          animations: { [weak self] in
                            if(seconds < 10){
                                self!.lblSeconds.text = seconds >= 0 ? "0\(seconds)" : "00"
                            }else{
                                self!.lblSeconds.text = "\(seconds)"
                            }
                            if minutes < 10 {
                                self!.lblMinutes.text = minutes >= 0 ? "0\(minutes)" : "00"
                            }else{
                                self!.lblMinutes.text = "\(minutes)"
                            }
                            if hours < 10 {
                                self!.lblHours.text = hours >= 0 ? "0\(hours)" : "00"
                            }else{
                                self!.lblHours.text = "\(hours)"
                            }
            }, completion: nil)
    }
    
    func eventPostsGetCall(){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.eventPostsGet(eventId: eventItem.id ?? 0)), completion: { apiResponse in

            switch apiResponse {
            case .Failure(_):
                break
                
            case .Success(let data):
                guard let results = data?.result as? [[String:Any]] else {
                    return
                }
                self.delighters.removeAll()
                for result in results {
                    let delighter = Delighter().initDelighter(from: result)
                    self.delighters.append(delighter)
                }
                self.tableView.reloadData()
                
                break
            }
        })
    }
    
//    func setTimeLabelSize(withRespectTo hours: Int) {
//        var labelSize: CGFloat = 51
//
//        if hours.digits.count < 3 {
//            labelSize = 51
//        } else if hours.digits.count == 3 {
//            labelSize = 46
//        } else if hours.digits.count == 4 {
//            labelSize = 35
//        } else if hours.digits.count == 5 {
//            labelSize = 25
//        } else if hours.digits.count >= 6 {
//            labelSize = 15
//        }
//
//        lblHours.font = FrenzyFont.kNunitoBold(labelSize).font()
//        lblMinutes.font = FrenzyFont.kNunitoBold(labelSize).font()
//        lblSeconds.font = FrenzyFont.kNunitoBold(labelSize).font()
//
//    }
    
    func onClickSponsorLink(){
        if var sponsorLink = eventItem.sponsor_link {
            if !sponsorLink.isEmpty {
                if !sponsorLink.starts(with: "http") {
                    sponsorLink = "https://" + sponsorLink
                }
            }
            if let url = URL(string: sponsorLink) {
                if !url.absoluteString.isEmpty {
                    UIApplication.shared.open(url, options: [:])
                }
            }
        }
    }
    
    override func onClickDismiss(_ sender: Any ) {
        if let nev = self.navigationController{
            for controller in nev.viewControllers as Array {
                if controller.isKind(of: HomeVC.self) {
                    nev.popToViewController(controller, animated: true)
                    break
                }
            }
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //useless funciton for now
    @IBAction func onClickSponsor(_ sender: Any) {
//        if var sponsorLink = eventItem.sponsor_link {
//            if !sponsorLink.isEmpty {
//                if !sponsorLink.starts(with: "http") {
//                    sponsorLink = "https://" + sponsorLink
//                }
//            }
//            if let url = URL(string: sponsorLink) {
//                if !url.absoluteString.isEmpty {
//                    UIApplication.shared.open(url, options: [:])
//                }
//            }
//        }
    }
    
    @IBAction func onClickSettings(_ sender: Any) {
             self.showSettingPopUp(isVenuHide: !self.eventItem.isGroundEvent){ (index) in
               if index ==  1{
                   self.openSettingsVC(with: self.eventItem)
               }else if index == 2{
                   let vc = EventTicketVC.instantiate(fromAppStoryboard: .home)
                   vc.eventItem = self.eventItem
                   vc.isFromSettings = true
                   self.present(vc, animated: true, completion: nil)
               }else if index == 3{
                     self.confirmationCustomeAlert(title: "", discription: "Are you sure you want to Exit Event?", btnColor1: UIColor.colorFromRGB(0xff1a57), btnColor2: UIColor.colorFromRGB(0xffffff),btnTitle1: "YES", btnTitle2: "NO", complition: { (confirm, tag) in
                                if confirm {
                                   self.showLoading(on: self.view, of: FrenzyColor.white)
                                   APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: "CALL `sp_eventExit`(\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0), \(self.eventItem.id!))"), completion: { apiResponse in
                                           switch apiResponse {
                                           case .Failure(_):
                                               self.showErrorToast(title: "Some thing wrong!", description: "Server not reponding properly, try again!") { (isDone) in
                                                   
                                               }
                                               break
                                           case .Success(_):
                                               let homeVC = HomeVC.instantiate(fromAppStoryboard: .home)
                                                self.navigationController?.pushViewController(homeVC, animated: true)
                                               break
                                           }
                                       })
                                }
                            })
               }
           }
    }
    
}

extension EventCountDownVC: UITableViewDelegate, UITableViewDataSource {
    
    func setupTable(){
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib.init(nibName: FrenzyCountCell.identifier, bundle: Bundle(for: FrenzyCountCell.self)), forCellReuseIdentifier: FrenzyCountCell.identifier)
        self.tableView.register(UINib.init(nibName: SponsorImageCell.identifier, bundle: Bundle(for: SponsorImageCell.self)), forCellReuseIdentifier: SponsorImageCell.identifier)
        self.tableView.register(UINib.init(nibName: SponsorLinkCell.identifier, bundle: Bundle(for: SponsorLinkCell.self)), forCellReuseIdentifier: SponsorLinkCell.identifier)
        self.tableView.register(UINib.init(nibName: FrenzyLogoCell.identifier, bundle: Bundle(for: FrenzyLogoCell.self)), forCellReuseIdentifier: FrenzyLogoCell.identifier)
        self.tableView.register(UINib.init(nibName: MovementSimpleCell.identifier, bundle: Bundle(for: MovementSimpleCell.self)), forCellReuseIdentifier: MovementSimpleCell.identifier)
        self.tableView.register(UINib.init(nibName: MovementSponserCell.identifier, bundle: Bundle(for: MovementSponserCell.self)), forCellReuseIdentifier: MovementSponserCell.identifier)
        self.tableView.register(UINib.init(nibName: MovementDetailCell.identifier, bundle: Bundle(for: MovementDetailCell.self)), forCellReuseIdentifier: MovementDetailCell.identifier)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return delighters.count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == delighters.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: FrenzyCountCell.identifier, for: indexPath) as! FrenzyCountCell
            cell.updateFrenzies(with: eventItem.total_viewers ?? 0)
            cell.selectionStyle = .none
            return cell
        }else if indexPath.row == delighters.count + 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: FrenzyLogoCell.identifier, for: indexPath) as!  FrenzyLogoCell
            cell.selectionStyle = .none
            return  cell
        }
        else {
            let delighter = delighters[indexPath.row]
            return getDelighterCell(on: indexPath, with: delighter)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != delighters.count + 1, indexPath.row != delighters.count {
            let delighter = delighters[indexPath.row]
            self.openDelighterVC(with: delighter, isFromTimeLine: true  , eventID: "\(eventItem.id ?? 0)" , bottomColor: eventItem.bottom_color ?? "")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
//        if indexPath.row == 1, (eventItem.sponsor_image ?? "").isEmpty, (eventItem.sponsor_link ?? "").isEmpty {
//            return 0
//        }
        return UITableView.automaticDimension
    }
    
    func getDelighterCell(on indexPath: IndexPath, with delighter: Delighter) -> UITableViewCell{
        
        if delighter.title != "", delighter.description != "" {
            //show detail cell
            let cell  = tableView.dequeueReusableCell(withIdentifier: MovementDetailCell.identifier, for: indexPath) as!  MovementDetailCell
            cell.delighter = delighter
            cell.configure()
            cell.imgHeight.constant =  CGFloat((Double(tableView.frame.size.width) * delighter.ratio!))
            cell.selectionStyle = .none
            return cell
        } else if delighter.description != "" {
            //show simplecell with description
            let cell  = tableView.dequeueReusableCell(withIdentifier: MovementSimpleCell.identifier, for: indexPath) as!  MovementSimpleCell
            cell.baseVC = self
            cell.delighter = delighter
            cell.configure(isDescription: true)
            cell.imgHeight.constant =  CGFloat((Double(tableView.frame.size.width) * delighter.ratio!))
            cell.selectionStyle = .none
            return  cell
            
        } else if delighter.title != "" {
            //show simplecell without description
            let cell = tableView.dequeueReusableCell(withIdentifier: MovementSimpleCell.identifier, for: indexPath) as!  MovementSimpleCell
            cell.baseVC = self
            cell.delighter = delighter
            cell.configure(isDescription: false)
              cell.imgHeight.constant =  CGFloat((Double(tableView.frame.size.width) * delighter.ratio!))
            cell.selectionStyle = .none
            return  cell
        } else {
            //show image/sponsor cell
            let cell = tableView.dequeueReusableCell(withIdentifier: MovementSponserCell.identifier, for: indexPath) as!  MovementSponserCell
            cell.baseVC = self
            cell.configure(image: delighter.image ?? "")
              cell.imgHeight.constant =  CGFloat((Double(tableView.frame.size.width) * delighter.ratio!))
            cell.selectionStyle = .none
            return  cell
        }
    }
    
}
