

import UIKit
import Kingfisher
import AVKit
import AVFoundation
protocol EventDismissDelegate {
    func removeEvventDetailView()
}

class EventDetailsVC: BaseViewController {
    static let identifier = "EventDetailsVC"
    @IBOutlet weak var switchScreen: UISwitchCustom!
    @IBOutlet weak var switchFlash: UISwitchCustom!
    @IBOutlet weak var switchVibrate: UISwitchCustom!
    @IBOutlet weak var teamsVieew: UIView!
    @IBOutlet weak var viewRight: UIView!
    @IBOutlet weak var viewLeft: UIView!
    @IBOutlet weak var sideMenu: UIView!
    @IBOutlet weak var imgTeamEvent: UIImageView!
    @IBOutlet weak var imgTeamLeft: UIImageView!
    @IBOutlet weak var imgTeamRight: UIImageView!
    @IBOutlet weak var imgFavRight: UIImageView!
    @IBOutlet weak var imgFavLeft: UIImageView!
    @IBOutlet weak var btnSubmit: TransitionButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tutorialCommentContainerView: UIView!
    //
    @IBOutlet weak var tutorialCommentSubContainerView: UIView!
    @IBOutlet weak var imgLeftHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgLeftWidthConstraint: NSLayoutConstraint!
    // MARK:- EventImageConstraint
    @IBOutlet weak var imgEventHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgEventWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var topViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var animateTutorailView: UIView!
    
    var eventItem:EventItem = EventItem()
    // MARK:- Cell Item
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var dateView: UIView!
    
    var eventDismissDelegate : EventDismissDelegate?
    var isLeftTeamFav = false
    var isFromSettings = false
    var timer: Timer?
    var isFromStartHereEvent : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = FrenzyColor.themeColor
        self.switchFlash.setWidth(width: 53, height: 31)
        self.switchScreen.setWidth(width: 53, height: 31)
        self.switchVibrate.setWidth(width: 53, height:31)
        self.switchFlash.addTarget(self, action: #selector(self.switchChanged), for: UIControl.Event.valueChanged)
        self.switchScreen.addTarget(self, action: #selector(self.switchChanged), for: UIControl.Event.valueChanged)
        self.switchVibrate.addTarget(self, action: #selector(self.switchChanged), for: UIControl.Event.valueChanged)
        self.setupViewsHeightAccordingToIphone()
        self.configure(item : self.eventItem)
        self.initializeFavTeam()
        self.setupBtnSubmit()
        self.setupSwitches()
        self.setupHeaderView()
        self.sideMenu.isHidden = true
        if isFromStartHereEvent{
            tutorialCommentContainerView.isHidden = false
            startAnimation()
           // self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.startAnimation), userInfo: nil, repeats: true)
        }else{
            tutorialCommentContainerView.isHidden = true
        }
       
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
       // self.tutorialCommentContainerView.layer.removeAllAnimations()
        self.timer?.invalidate()
    }
    
    deinit {
   
    }
    
    @objc func startAnimation(){
        startTutorialANimation(tutorialCommentContainerView, nil)
    }
    
    
    func initializeFavTeam() {
        self.imgFavLeft.image = UIImage(named: "heart", in: Bundle(for: EventDetailsVC.self), with: nil)
        self.imgFavRight.image = UIImage(named: "like", in: Bundle(for: EventDetailsVC.self), with: nil)
        self.teamsVieew.bringSubviewToFront( self.viewRight)
        isLeftTeamFav = false
    }
    
    func setupBtnSubmit() {
        self.btnSubmit.cornerRadiusButton = 29
        self.btnSubmit.spinnerColor = .white
        if isFromSettings {
            self.btnSubmit.setTitle("SUBMIT", for: .normal)
        } else {
            self.btnSubmit.setTitle("SUBMIT", for: .normal)
        }
    }
    
    func setupHeaderView() {
        if isFromSettings {
            headerView.isHidden = false
            topViewHeightConstraint.constant = 60
        } else {
            headerView.isHidden = true
            topViewHeightConstraint.constant = 0
        }
    }
    
    func setupSwitches() {
        if isFromSettings {
            switchFlash.setOn(eventItem.canFlash, animated: false)
            switchFlash.thumbTintColor = switchFlash.isOn ? FrenzyColor.redColor : .white
            switchScreen.setOn(eventItem.canChangeScreen, animated: false)
            switchScreen.thumbTintColor = switchScreen.isOn ? FrenzyColor.redColor : .white
            switchVibrate.setOn(eventItem.canVibrate, animated: false)
            switchVibrate.thumbTintColor = switchVibrate.isOn ? FrenzyColor.redColor : .white
        }
    }
    
    func apiCall(){
        
    }
    
    func configure(item : EventItem) {
        switch(item.eventCategory){
        case .MATCH:
            //
            self.imgTeamEvent.isHidden = true
            self.viewRight.isHidden = !self.imgTeamEvent.isHidden
            self.viewLeft.isHidden = !self.imgTeamEvent.isHidden
            self.imgTeamLeft.kf.setImage(with: URL.init(string: item.team1_logo ?? ""), placeholder: UIImage(named: "placeholder", in: Bundle(for: EventDetailsVC.self) , with: nil))
            self.imgTeamRight.kf.setImage(with: URL.init(string: item.team2_logo ?? ""), placeholder: UIImage(named: "placeholder", in: Bundle(for: EventDetailsVC.self) , with: nil))
            //
            self.setAttributedTextForLabel(mainString: "\(item.teamOne ?? "") at \(item.teamTwo ?? "")  ", attributedStringsArray: ["at"], lbl: self.lblTitle, color: FrenzyColor.grayKindColor, attFont: FrenzyFont.kNunitoRegular(8.9).font())
            self.setAttributedTextForLabel(mainString: "\(item.time ?? "")   \(item.place ?? "")   \(item.location ?? "")", attributedStringsArray: [item.place ?? ""], lbl: self.lblDetail, color: FrenzyColor.purpleColor, attFont: FrenzyFont.kNunitoRegular(8.9).font())
            self.dateView.backgroundColor = FrenzyColor.greenColor//
            break
        case .EVENT:
            //
            self.imgTeamEvent.isHidden = false
            self.viewRight.isHidden = !self.imgTeamEvent.isHidden
            self.viewLeft.isHidden = !self.imgTeamEvent.isHidden
            imgTeamEvent.kf.setImage(with: URL.init(string: item.team1_logo ?? ""), placeholder: UIImage(named: "placeholder", in: Bundle(for: EventDetailsVC.self) , with: nil))
            //
            self.dateView.backgroundColor = FrenzyColor.frozyKindColor
            self.setAttributedTextForLabel(mainString: "\(item.eventName ?? "") \(item.eventPlace ?? "")", attributedStringsArray: [item.eventPlace ?? ""], lbl: self.lblTitle, color: FrenzyColor.grayKindColor, attFont: FrenzyFont.kNunitoRegular(8.9).font())
            self.setAttributedTextForLabel(mainString: "\(item.time ?? "")   \(item.place ?? "")   \(item.location ?? "")", attributedStringsArray: [item.place ?? ""], lbl: self.lblDetail, color: FrenzyColor.purpleColor, attFont: FrenzyFont.kNunitoRegular(8.9).font())
            break
        case .none:
            break
        }
       
    }
    
    func setupViewsHeightAccordingToIphone(){
        switch (self.eventItem.eventCategory){
        case .EVENT:
            self.imgLeftHeightConstraint.constant = 0
            self.imgLeftWidthConstraint.constant = self.imgLeftHeightConstraint.constant
            self.imgTeamLeft.cornerRadius = 0
            self.imgTeamRight.cornerRadius = self.imgTeamLeft.cornerRadius
            switch(checkIphoneIs()){
                
                case .IPHONE_5_5S_5C,.IPHONE_6_6S_7_8:
                    self.imgEventHeightConstraint.constant = 120
                    self.imgEventWidthConstraint.constant = self.imgEventHeightConstraint.constant
                    self.imgTeamEvent.cornerRadius = 60
                    break
                case .IPHONE_6PLUS_6SPLUS_7PLUS_8PLUS:
                    self.imgEventHeightConstraint.constant = 130
                    self.imgEventWidthConstraint.constant = self.imgEventHeightConstraint.constant
                    self.imgTeamEvent.cornerRadius = 65
                    
                    break
                case .IPHONE_X_XS_11_Pro,.IPHONE_XS_Max_11_Pro_Max,.IPHONE_XR_11:
                    self.imgEventHeightConstraint.constant = 155
                    self.imgEventWidthConstraint.constant = self.imgEventHeightConstraint.constant
                    self.imgTeamEvent.cornerRadius = 77.5
                    break
                case .UNKNOWN:
                    break
            }
            break
        case .MATCH:
            self.imgEventHeightConstraint.constant = 0
            self.imgEventWidthConstraint.constant = self.imgEventHeightConstraint.constant
            self.imgTeamEvent.cornerRadius = 0
            switch(checkIphoneIs()){
                case .IPHONE_5_5S_5C,.IPHONE_6_6S_7_8:
                    self.imgLeftHeightConstraint.constant = 100
                    self.imgLeftWidthConstraint.constant = self.imgLeftHeightConstraint.constant
                    self.imgTeamLeft.cornerRadius = 50
                    self.imgTeamRight.cornerRadius = self.imgTeamLeft.cornerRadius
                    break
                case .IPHONE_6PLUS_6SPLUS_7PLUS_8PLUS:
                    self.imgLeftHeightConstraint.constant = 110
                    self.imgLeftWidthConstraint.constant = self.imgLeftHeightConstraint.constant
                    self.imgTeamLeft.cornerRadius = 55
                    self.imgTeamRight.cornerRadius = self.imgTeamLeft.cornerRadius
                    break
                case .IPHONE_X_XS_11_Pro,.IPHONE_XS_Max_11_Pro_Max,.IPHONE_XR_11:
                    self.imgLeftHeightConstraint.constant = 125
                    self.imgLeftWidthConstraint.constant = self.imgLeftHeightConstraint.constant
                    self.imgTeamLeft.cornerRadius = 62.5
                    self.imgTeamRight.cornerRadius = self.imgTeamLeft.cornerRadius
                    break
                case .UNKNOWN:
                    break
            }
            break
        case .none:
            break
        }
        
    }
    
    @objc func switchChanged(mySwitch: UISwitch) {
        let value = mySwitch.isOn
        
        if #available(iOS 13.0, *){
            switch (mySwitch){
            case self.switchVibrate:
                
                _  = value ? VibrateFrenzy.vibrate(for: 1) : VibrateFrenzy.vibrate(for: 0)
                self.switchVibrate.backgroundColor = .white
                self.switchVibrate.thumbTintColor = value ? FrenzyColor.redColor : .white
                print("vibrateStatus iOS 13.0 :\(value)")
                break
            case self.switchFlash:
                self.switchFlash.backgroundColor = .white
                self.switchFlash.thumbTintColor = value ? FrenzyColor.redColor : .white
                _ = value ? FlashFrenzy.flash(for: 2) : FlashFrenzy.offFlash()
                break
            case self.switchScreen:
                self.switchScreen.backgroundColor = .white
                self.switchScreen.thumbTintColor = value ? FrenzyColor.redColor : .white
                if !isFromSettings {
                    _ = value ? (self.parent?.view.backgroundColor = FrenzyColor.purpleDarkColor) : (self.parent?.view.backgroundColor = FrenzyColor.yellowColor)
                    self.view.backgroundColor = self.parent?.view.backgroundColor
                    if(value){
                        self.delay(0.5, completion: {
                            self.parent?.view.backgroundColor = FrenzyColor.yellowColor
                            self.view.backgroundColor = self.parent?.view.backgroundColor
                        })
                    }
                } else {
                    _ = value ? (self.view.backgroundColor = FrenzyColor.purpleDarkColor) : (self.view.backgroundColor = FrenzyColor.yellowColor)
                    if(value){
                        self.delay(0.5, completion: {
                            self.view.backgroundColor = FrenzyColor.yellowColor
                        })
                    }
                }
                break
            default:
                break
            }
        }else{
            
            switch (mySwitch){
            case self.switchVibrate:
                self.switchVibrate.backgroundColor = .white
                self.switchVibrate.thumbTintColor = value ? FrenzyColor.redColor : FrenzyColor.offSwtichThumbColor
                break
            
            case self.switchFlash:
                _ = value ? FlashFrenzy.flash(for: 2) : FlashFrenzy.offFlash()
                self.switchFlash.backgroundColor = .white
                self.switchFlash.thumbTintColor = value ? FrenzyColor.redColor : FrenzyColor.offSwtichThumbColor
                break
            case self.switchScreen:
                self.switchScreen.backgroundColor = .white
                self.switchScreen.thumbTintColor = value ? FrenzyColor.redColor : FrenzyColor.offSwtichThumbColor
                _ = value ? (self.parent?.view.backgroundColor = FrenzyColor.purpleDarkColor) : (self.parent?.view.backgroundColor = FrenzyColor.yellowColor)
                self.view.backgroundColor = self.parent?.view.backgroundColor
                if(value){
                    self.delay(0.5, completion: {
                        self.parent?.view.backgroundColor = FrenzyColor.yellowColor
                        self.view.backgroundColor = self.parent?.view.backgroundColor
                    })
                }
                break
            default:
                break
            }
        }
        
        // Do something
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setupBettryIconBlackOrWhite()
    }
    
    func getFavTeamId() -> Int {
        
        if eventItem.eventCategory ?? EventCategory.EVENT == .MATCH {
        
            if isLeftTeamFav {
                return eventItem.team1_id ?? 0
            } else {
                return eventItem.team2_id ?? 0
            }
            
        } else {
            return eventItem.team1_id ?? 0
        }
        
    }
    
    func openNextController() {
        
        eventItem.is_flash = switchFlash.isOn ? 1 : 0
        eventItem.is_screen = switchScreen.isOn ? 1 : 0
        eventItem.is_vibrate = switchVibrate.isOn ? 1 : 0
        updatePreferences()
        
        self.dismiss(animated: false, completion: nil)
        
        let vc = EventTicketVC.instantiate(fromAppStoryboard: .home)
        vc.eventItem = self.eventItem
        vc.isFromStartHereEvent = isFromStartHereEvent
        if isFromStartHereEvent{
            demoEventId = eventItem.id ?? 0
        }
        vc.isFromSettings = isFromSettings
        self.presentingViewController?.present(vc, animated: true, completion: nil)
    }
    
    func updatePreferences() {
        let eventIdArr = UserDefaults.standard.array(forKey: kEventIdKey) as? [Int] ?? [Int]()
        var flashArr = UserDefaults.standard.array(forKey: kFlashKey) as? [Bool] ?? [Bool]()
        var screenArr = UserDefaults.standard.array(forKey: kScreenKey) as? [Bool] ?? [Bool]()
        var vibrateArr = UserDefaults.standard.array(forKey: kVibrateKey) as? [Bool] ?? [Bool]()
        
        for (index,eventId) in eventIdArr.enumerated() {
            if eventId == self.eventItem.id ?? 0 {
                flashArr[index] = switchFlash.isOn
                screenArr[index] = switchScreen.isOn
                vibrateArr[index] = switchVibrate.isOn
                break
            }
        }
        
        UserDefaults.standard.removeObject(forKey: kScreenKey)
        UserDefaults.standard.removeObject(forKey: kFlashKey)
        UserDefaults.standard.removeObject(forKey: kVibrateKey)
        
        print("Saved EventID Arr: \(eventIdArr)")
        
        UserDefaults.standard.set(flashArr, forKey: kFlashKey)
        UserDefaults.standard.set(screenArr, forKey: kScreenKey)
        UserDefaults.standard.set(vibrateArr, forKey: kVibrateKey)
    }
    
    func addPreferences() {
        
        eventItem.is_flash = switchFlash.isOn ? 1 : 0
        eventItem.is_screen = switchScreen.isOn ? 1 : 0
        eventItem.is_vibrate = switchVibrate.isOn ? 1 : 0
        
        var eventIdArr = UserDefaults.standard.array(forKey: kEventIdKey) as? [Int] ?? [Int]()
        var sectionIdArr = UserDefaults.standard.array(forKey: kSectionIdKey) as? [Int] ?? [Int]()
        var isFlashArr = UserDefaults.standard.array(forKey: kFlashKey) as? [Bool] ?? [Bool]()
        var isScreenArr = UserDefaults.standard.array(forKey: kScreenKey) as? [Bool] ?? [Bool]()
        var isVibrateArr = UserDefaults.standard.array(forKey: kVibrateKey) as? [Bool] ?? [Bool]()
        
        eventIdArr.append(eventItem.id ?? 0)
        sectionIdArr.append(0)      //dummy section id so that it should be updated later
        isFlashArr.append(switchFlash.isOn)
        isScreenArr.append(switchScreen.isOn)
        isVibrateArr.append(switchVibrate.isOn)
        
        print("Saved EventID Arr: \(eventIdArr)")
        UserDefaults.standard.set(eventIdArr, forKey: kEventIdKey)
        UserDefaults.standard.set(sectionIdArr, forKey: kSectionIdKey)
        UserDefaults.standard.set(isFlashArr, forKey: kFlashKey)
        UserDefaults.standard.set(isScreenArr, forKey: kScreenKey)
        UserDefaults.standard.set(isVibrateArr, forKey: kVibrateKey)
        
        UserDefaults.standard.synchronize()
    }
    
    func submitEventSettingsCall() {
        btnSubmit.startAnimation()
        
        APIManager.sharedInstance.opertationWithRequest(withApi: API.attendEvent(viewer_id: "\(DataManager.sharedInstance.getPermanentlySavedUser()?.userItem.id ?? 0)", event_id: "\(eventItem.id ?? 0)", team_id: "\(getFavTeamId())", is_vibrate: switchVibrate.isOn ? "1" : "0", is_flash: switchFlash.isOn ? "1" : "0", is_screen: switchScreen.isOn ? "1" : "0")) { [self] (APIresponse) in
           // NotificationCenter.default.post(name: .homeFeedRefresh, object: nil)
            switch APIresponse {
            case .Failure(_):
                self.btnSubmit.stopAnimation()
                break
            case .Success(_):
                
                if self.isFromSettings {
                    self.btnSubmit.stopAnimation()
                    if self.eventItem.isGenralSeats ?? false {
                        self.dismiss(animated: true, completion: nil)
                    }else{
                        if self.eventItem.isGroundEvent {
                            self.openNextController()
                        }else{
                            self.openNextController()
                           // self.dismiss(animated: true, completion: nil)
                        }
                        
                    }
                } else {
                    if self.eventItem.isGenralSeats ?? false {
                        self.addPreferences()
                        self.getEventSections()
                    }else{
                        self.addPreferences()
                        // Add Far Away
                        if self.eventItem.isGroundEvent {
                            self.btnSubmit.stopAnimation()
                            let vc = EventTicketVC.instantiate(fromAppStoryboard: .home)
                            vc.eventItem = self.eventItem
                            vc.isFromStartHereEvent = self.isFromStartHereEvent
                            if isFromStartHereEvent{
                                demoEventId = eventItem.id ?? 0
                            }
                            self.navigationController?.pushViewController(vc, animated: true)
                        }else {
                            self.remoteEvent()
                        }
                    }
                }
                break
            }
        }
    }
    
    func remoteEvent() {
        APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.eventTicketUpdate(eventId: eventItem.id, section: 0, level: "A" ,row: "0", seatNo: 0, isVenu: 0)), completion: { apiResponse in
            self.btnSubmit.stopAnimation()
            switch apiResponse {
            case .Failure(_):
                break
            case .Success( _):
                 self.btnSubmit.stopAnimation()
                self.openNextControllerRemote()
                break
            }
            
        })
    }
    
    func openNextControllerRemote() {
          switch eventItem.eventStatus {
          case .UPCOMING:
              let vc = EventCountDownVC.instantiate(fromAppStoryboard: .home)
              vc.eventItem = self.eventItem
              self.navigationController?.pushViewController(vc, animated: true)
              
          case .ONGOING:
              let vc = EventMovementVC.instantiate(fromAppStoryboard: .home)
              vc.eventItem = self.eventItem
              vc.eventTitle = self.eventItem.title ?? ""
              self.navigationController?.pushViewController(vc, animated: true)
              
          case .ENDED:
              let vc = EventSummeryVC.instantiate(fromAppStoryboard: .Movement)
              vc.eventItem = self.eventItem
              self.navigationController?.pushViewController(vc, animated: true)
          }
      }
    
    func getEventSections(){
        APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.eventSectionGet(eventId: eventItem.id ?? 0)), completion: { apiResponse in
            switch apiResponse {
            case .Failure(_):
                self.showErrorToast(title: "Invalid Data!", description: "we are unable to proceed your request, try again!") { (ok) in
                }
                break
            case .Success(let data):
                guard let results = data?.result as? [[String:Any]] else {
                    return
                }
                if results.count > 0 {
                    var seccs = [Section]()
                    for result in results {
                        let data = Section().initSections(from: result)
                        seccs.append(data)
                    }
                    self.submitEventTicketCall(sec: seccs)
                }else{
                    self.showErrorToast(title: "Invalid Data!", description: "we are unable to proceed your request, try again!") { (ok) in
                    }
                }
                break
            }
        })
    }
    
    func submitEventTicketCall(sec : [Section]) {
        let eventId = eventItem.id ?? 0
        let sectionId = sec[0].id ?? 0
        APIManager.sharedInstance.opertationWithRequest(withApi: API.callForProcedure(query: APIProcedure.eventTicketUpdate(eventId: eventId, section: sectionId, level: "", seatNo: 0, isVenu: 0)), completion: { apiResponse in
            self.btnSubmit.stopAnimation()
            switch apiResponse {
            case .Failure(_):
                break
            case .Success( _):
                self.updatePreferences(with: sectionId)
                self.openNextControllerWithSectionSetting()
                break
            }
            
        })
    }
    
    func updatePreferences(with sectionId: Int){
           let eventIdArr = UserDefaults.standard.array(forKey: kEventIdKey) as? [Int] ?? [Int]()
           var sectionIdArr = UserDefaults.standard.array(forKey: kSectionIdKey) as? [Int] ?? [Int]()
           
           for (index,eventId) in eventIdArr.enumerated() {
               if eventId == self.eventItem.id ?? 0 {
                   sectionIdArr[index] = sectionId
                   break
               }
           }
            print("Saved EventID Arr: \(eventIdArr)")
           UserDefaults.standard.removeObject(forKey: kSectionIdKey)
           UserDefaults.standard.set(sectionIdArr, forKey: kSectionIdKey)
       }
    
    
    
    func openNextControllerWithSectionSetting() {
        switch eventItem.eventStatus {
        case .UPCOMING:
            let vc = EventCountDownVC.instantiate(fromAppStoryboard: .home)
            vc.eventItem = self.eventItem
            self.navigationController?.pushViewController(vc, animated: true)
            
        case .ONGOING:
            let vc = EventMovementVC.instantiate(fromAppStoryboard: .home)
            vc.eventItem = self.eventItem
            vc.eventTitle = self.eventItem.title ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
            
        case .ENDED:
            let vc = EventSummeryVC.instantiate(fromAppStoryboard: .Movement)
            vc.eventItem = self.eventItem
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func frenziesParamDictionary() -> [String: AnyObject] {
        var data = [String: AnyObject]()
        data["event_id"] = eventItem.id as AnyObject
        data["total_viewers"] = ((eventItem.total_viewers ?? 0) + 1) as AnyObject
        return data
    }
    
    
    @IBAction func onClickSubmit(_ sender: Any) {
        submitEventSettingsCall()
    }
    
    @IBAction func onClickCross(_ sender: Any) {
        if isFromSettings {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.eventDismissDelegate?.removeEvventDetailView()
        }
    }
    
    @IBAction func onClickLeftTeam(_ sender: Any) {
        self.imgFavLeft.image = UIImage(named: "like", in: Bundle(for: EventDetailsVC.self), with: nil)
        self.imgFavRight.image = UIImage(named: "heart", in: Bundle(for: EventDetailsVC.self), with: nil)
        self.imgTeamLeft.bounceAnimation()
        self.teamsVieew.bringSubviewToFront( self.viewLeft)
        isLeftTeamFav = true
    }
    
    @IBAction func onClickRightTeam(_ sender: Any) {
        self.imgFavLeft.image = UIImage(named: "heart", in: Bundle(for: EventDetailsVC.self), with: nil)
        self.imgFavRight.image = UIImage(named: "like", in: Bundle(for: EventDetailsVC.self), with: nil)
        self.imgTeamRight.bounceAnimation()
        self.teamsVieew.bringSubviewToFront( self.viewRight)
        isLeftTeamFav = false
    }
    
    var eventLogOutDone:(() -> Void)?
   
    @IBAction func onClickSideMenu(_ sender: Any){
        
    }
    
    @IBAction func onClickVideoDemo(_ sender: Any){
        guard let videoURL = URL(string: "\(APIConstants.BasePath)/Frenzy_HowTo_v03_final.mp4") else {
            return
        }
        try? AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
        let player = AVPlayer(url: videoURL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
}

